# uncomment these lines to get more pulse types:
#
# N is the number of desired pulse types
# epicsEnvSet("SIS8300LLRF_PULSETYPES" "N")
#
# pulse type zero is loaded always, if more are required, than
# requireSnippet(sis8300llrfLoadTablePair.cmd, "PULSE_TYPE=X")
# must be called N - 1 times, where 0 < X < N and N = number of pulse types

# common snippet must ALWAYS be loaded first!
requireSnippet(sis8300llrf-load-common.cmd)
requireSnippet(sis8300llrf-load-setup.cmd)
requireSnippet(sis8300llrf-load-specialModes.cmd)
requireSnippet(sis8300llrf-load-registers.cmd)
