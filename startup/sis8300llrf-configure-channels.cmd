################################
###### Runtime AI channel config
#enable normal ADC channels
dbpf $(LLRF_PREFIX):AI2-ENBL    1
dbpf $(LLRF_PREFIX):AI3-ENBL    1
dbpf $(LLRF_PREFIX):AI4-ENBL    1
dbpf $(LLRF_PREFIX):AI5-ENBL    1

#set internal channel linear conversion
dbpf $(LLRF_PREFIX):AI6-LCVF    1
dbpf $(LLRF_PREFIX):AI7-LCVF    1
dbpf $(LLRF_PREFIX):AI8-LCVF    1
dbpf $(LLRF_PREFIX):AI9-LCVF    1
dbpf $(LLRF_PREFIX):AI6-LCVO    0
dbpf $(LLRF_PREFIX):AI7-LCVO    0
dbpf $(LLRF_PREFIX):AI8-LCVO    0
dbpf $(LLRF_PREFIX):AI9-LCVO    0
