###########
### Control table (SP or FF) channel for a pulse type. 
###########
# MACROS:
# PULSE_TYPE is equal to the channel number and ASYN_ADDR
# CTRL_TABLE_TYPE can be either FF or SP and is equal to the channel group name -> ASYN_PREFIX
# TABLE_SMNM_MAX should be equal to max allowed on the device and the one set in the 
#                          channel group. Currently 2^19
###########

record(bo, "$(PREFIX):$(CTRL_TABLE_TYPE)-$(CHAN_NAME=PT$(PULSE_TYPE))-WRTBL") {
    field(DESC, "Trigger write of tables")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) WriteTable")
}

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-$(CHAN_NAME=PT$(PULSE_TYPE))-Q") {
    field(DESC, "Write the Q control table.")
    field(DTYP, "asynFloat32ArrayOut")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) QTable")
    field(FTVL, "FLOAT")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-$(CHAN_NAME=PT$(PULSE_TYPE))-I") {
    field(DESC, "Write the I control table.")
    field(DTYP, "asynFloat32ArrayOut")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) ITable")
    field(FTVL, "FLOAT")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(longin, "$(PREFIX):$(CTRL_TABLE_TYPE)-$(CHAN_NAME=PT$(PULSE_TYPE))-SMNM-RBV") {
    field(DESC, "Number of elements in the control table")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) SamplesCount")
    field(SCAN, "I/O Intr")
}

########
## READ TABLES FROM HW ON REQUEST
## Mostly used for debugging, not as pulse to pulse

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-$(CHAN_NAME=PT$(PULSE_TYPE))-RAWTABLE-GET") {
    field(DESC, "get the raw table from HW.")
    field(DTYP, "asynInt32ArrayIn")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) RawTable")
    field(FTVL, "ULONG")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-$(CHAN_NAME=PT$(PULSE_TYPE))-I-GET") {
    field(DESC, "Get the I control table.")
    field(DTYP, "asynFloat32ArrayIn")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) ITable")
    field(FTVL, "FLOAT")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-$(CHAN_NAME=PT$(PULSE_TYPE))-Q-GET") {
    field(DESC, "Get the Q control table.")
    field(DTYP, "asynFloat32ArrayIn")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) QTable")
    field(FTVL, "FLOAT")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

########
# Channel Status
########
record(mbbi, "$(PREFIX):$(CTRL_TABLE_TYPE)-$(CHAN_NAME=PT$(PULSE_TYPE))-STAT") {
    field(DESC, "The state of the channel.")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) State")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "UNKNOWN")
    field(ONVL, "1")
    field(ONST, "IOCINIT")
    field(TWVL, "2")
    field(TWST, "DISABLED")
    field(THVL, "3")
    field(THST, "ENABLED")
    field(FRVL, "4")
    field(FRST, "PROCESSING")
    field(FVVL, "5")
    field(FVST, "DEGRADED")
    field(FVSV, "MAJOR")
    field(SXVL, "6")
    field(SXST, "ERROR")
    field(SXSV, "MAJOR")
    field(SVVL, "7")
    field(SVST, "RESETTING")
    field(EIVL, "8")
    field(EIST, "DEFUNCT")
    field(EISV, "MAJOR")
    field(NIVL, "9")
    field(NIST, "READY")
}
