###########
### Control table (SP or FF) channel for configuring the Special Operating mode
###########
# MACROS:
# PULSE_TYPE is equal to the channel number and ASYN_ADDR
# CTRL_TABLE_TYPE can be either FF or SP
# CSTL_TABLE_CG_NAME is either ff orf sp (non-capital letters!)
# TABLE_SMNM_MAX should be equal to max allowed on the device and the one set in the 
#                          channel group. Currently 2^19
###########

record(bo, "$(PREFIX):$(CTRL_TABLE_TYPE)-SM-FFTABLEMODE") {
    field(DESC, "FF TAble Mode select")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) FFTableMode")
    field(ZNAM, "hold last")
    field(ONAM, "circular")
}

record(bi, "$(PREFIX):$(CTRL_TABLE_TYPE)-SM-FFTABLEMODE-RBV") {
    field(DESC, "FF TAble Mode RBV")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) FFTableMode")
    field(ZNAM, "hold last")
    field(ONAM, "circular")
    field(SCAN, "I/O Intr")
}

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-SM-ANG") {
    field(DESC, "Write the ANG control table.")
    field(DTYP, "asynFloat32ArrayOut")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) AngleTable")
    field(FTVL, "FLOAT")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-SM-MAG") {
    field(DESC, "Write the MAG control table.")
    field(DTYP, "asynFloat32ArrayOut")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) MagTable")
    field(FTVL, "FLOAT")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(longin, "$(PREFIX):$(CTRL_TABLE_TYPE)-SM-MAG-SMNM-RBV") {
    field(DESC, "Number of elements in the control table")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) SamplesCount")
    field(SCAN, "I/O Intr")
}

record(longin, "$(PREFIX):$(CTRL_TABLE_TYPE)-SM-ANG-SMNM-RBV") {
    field(DESC, "Number of elements in the control table")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) SamplesCount")
    field(SCAN, "I/O Intr")
}

########
## READ TABLES FROM HW ON REQUEST
## Mostly used for debugging, not as pulse to pulse
########

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-SM-MAG-GET") {
    field(DESC, "Get the MAg control table.")
    field(DTYP, "asynFloat32ArrayIn")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) MagTable")
    field(FTVL, "FLOAT")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(waveform, "$(PREFIX):$(CTRL_TABLE_TYPE)-SM-ANG-GET") {
    field(DESC, "Get the ANG control table.")
    field(DTYP, "asynFloat32ArrayIn")
    field(INP,  "@asyn($(ASYN_PORT).$(CTRL_TABLE_CG_NAME), $(PULSE_TYPE)) AngleTable")
    field(FTVL, "FLOAT")
    field(NELM, "$(TABLE_SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}


