#!/usr/bin/env python3
from helper import read_reg, change_state
from epics import caget, caput


class TestVM:
    def test_output_en(self, board, pref):
        res = caput(pref + ":VMENBL", 1)
        assert res == 1
        reg = read_reg(board, "0x12F")
        assert reg == "0x700"

    def test_output_dis(self, board, pref):
        res = caput(pref + ":VMENBL", 0)
        assert res == 1
        reg = read_reg(board, "0x12F")
        assert reg == "0x600"

class TestState:
    def test_states(self, pref):
        for state in ["RESET", "OFF", "INIT", "ON", "RESET", "INIT"]:
            assert change_state(pref, state)
