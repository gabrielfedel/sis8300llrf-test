from helper import read_reg
from epics import caget

def test_params(board, pref):
    assert caget(pref) is not None  # if fails, prefix invalid
    assert (read_reg(board, 0x0)).rfind("error") == -1 # if fail board invalid
