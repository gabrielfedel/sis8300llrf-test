from random import random
from time import sleep

from epics import caput
from helper import SIS8300DRV_RTM_ATT_VM, change_state, float_to_fixed, read_reg


class TestAttenuation:
    def set_rand_att(self, board, pref, ch):
        if ch < SIS8300DRV_RTM_ATT_VM :
            m = 5
            n = 1
        else:
            m = 4
            n = 2
        # generate a random value
        max_value = 2**m - 2**-n
        att_val = random()*max_value

        # set value
        caput(pref + ":AI" + str(ch) + "-ATT", str(att_val))
        valqmn = float_to_fixed(att_val, m, n, 0)

        # read from register
        reg = "0xF8" + str(2 + (ch//4))
        sleep(1)
        res = int(read_reg(board, reg), 16)

        # get the desired part
        rshift = ch % 4 * 8
        res = (res >> rshift) & 0x000000FF

        return valqmn == res
   
    def test_init(self, board, pref):
        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)

        for ch in range(0,9):
            assert self.set_rand_att(board, pref, ch) == True

    def test_on(self, board, pref):
        assert change_state(pref, "ON")

        for ch in range(0,9):
            assert self.set_rand_att(board, pref, ch) == True

    def test_after_reset(self, board, pref):
        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)

        for ch in range(0,9):
            assert self.set_rand_att(board, pref, ch) == True

