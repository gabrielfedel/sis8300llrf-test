import pytest
from random import randrange
from time import sleep

from epics import caget
from helper import change_state, sim_bp_trig

class TestSimBPTrig:
    @pytest.mark.parametrize("pulses", [50, 49, randrange(1, 50)])
    def test_bp_trig(self, board, pref, pulses):
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(pref, state)

        for i in range(pulses):
            sim_bp_trig(board)
        
        res = caget(pref + ":PULSEDONECNT")
        assert pulses == res

        sleep(2)
        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)
