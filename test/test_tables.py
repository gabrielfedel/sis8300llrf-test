#!/usr/bin/env python3
from random import random, randint

from helper import FF_REG, SP_REG, read_reg, float_to_fixed, get_min_max_mn, read_mem
from epics import caput

NELM = 16

# Normal mode

# SP - PT0 - I
# SP - PT0 - Q
# FF - PT0 - I
# FF - PT0 - Q 

# Special mode

# SP - SM - I
# SP - SM - Q
# FF - SM - I
# FF - SM - Q 

def set_table(pref, ctrl, typet, qi, qmn, size = 16):
    """Set values for one table (the same value)
    ctrl = SP / FF
    typet = PT0 / SM
    qi = I / Q
    size = number of elements
    qmn = (m, n, signed)
    """
    (min, max) = get_min_max_mn(*qmn)

    val = round(random()*(max-min)+min, qmn[1])
    while (val > max or val < min):
        val = round(random()*(max-min)+min, qmn[1])

    vals = []
    vals.extend([val]*size)
    
    pv = pref + ":" + ctrl + "-" + typet

    caput(pv + "-" + qi, vals)

    # write table to the memory
    caput(pv + "-WRTBL", 1)

    # update readback
    caput(pv + "-" + qi + "-GET.PROC", 1)

    return val

class TestTablesNormal:
    """Class for test tables on Normal mode"""
    def test_tables_sp_qi(self, board, pref):
        """Test tables SP Q/I on normal mode"""
        qmn = (1, 15, 1)
        q_val = set_table(pref, "SP", "PT0", "Q", qmn)
        i_val = set_table(pref, "SP", "PT0", "I", qmn)
    
        mem_pos = int(read_reg(board, SP_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        q_val_qmn = str(float_to_fixed(q_val, *qmn))
        i_val_qmn = str(float_to_fixed(i_val, *qmn))
       
        # get a random position to check 
        pos = randint(1, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2]
        assert i_val_qmn == mem_values[pos*2+1]

    def test_tables_ff_qi(self, board, pref):
        """Test tables FF Q/I on normal mode"""
        qmn = (1, 15, 1)
        q_val = set_table(pref, "FF", "PT0", "Q", qmn)
        i_val = set_table(pref, "FF", "PT0", "I", qmn)
    
        mem_pos = int(read_reg(board, FF_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        q_val_qmn = str(float_to_fixed(q_val, *qmn))
        i_val_qmn = str(float_to_fixed(i_val, *qmn))
       
        # get a random position to check 
        pos = randint(1, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2]
        assert i_val_qmn == mem_values[pos*2+1]


class TestTablesSpecial():
    """Class for test tables on Normal mode on normal mode"""
    def test_tables_sp_mag_ang(self, board, pref):
        """Test tables SP Mag and Ang"""
        qmn_ang = (3, 13, 1)
        qmn_mag_sp = (0, 16, 0)

        ang_val = set_table(pref, "SP", "SM", "ANG", qmn_ang)
        mag_val = set_table(pref, "SP", "SM", "MAG", qmn_mag_sp)
    
        mem_pos = int(read_reg(board, SP_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        ang_val_qmn = str(float_to_fixed(ang_val, *qmn_ang))
        mag_val_qmn = str(float_to_fixed(mag_val, *qmn_mag_sp))
       
        # get a random position to check 
        pos = randint(0, 15)
        # ang pos = pos*2
        # mag pos = pos*2 + 1

        assert ang_val_qmn == mem_values[pos*2]
        assert mag_val_qmn == mem_values[pos*2+1]

    def test_tables_ff_mag_ang(self, board, pref):
        """Test tables FF Mag and Ang"""
        qmn_ang = (3, 13, 1)
        qmn_mag_ff = (1, 15, 1)

        ang_val = set_table(pref, "FF", "SM", "ANG", qmn_ang)
        mag_val = set_table(pref, "FF", "SM", "MAG", qmn_mag_ff)
    
        mem_pos = int(read_reg(board, FF_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        ang_val_qmn = str(float_to_fixed(ang_val, *qmn_ang))
        mag_val_qmn = str(float_to_fixed(mag_val, *qmn_mag_ff))
       
        # get a random position to check 
        pos = randint(0, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert ang_val_qmn == mem_values[pos*2]
        assert mag_val_qmn == mem_values[pos*2+1]

    def test_tables_sp_qi(self, board, pref):
        """Test tables SP Q and I """
        qmn_qi = (1, 15, 1)
        q_val = set_table(pref, "SP", "SM", "Q", qmn_qi)
        i_val = set_table(pref, "SP", "SM", "I", qmn_qi)
    
        mem_pos = int(read_reg(board, SP_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        q_val_qmn = str(float_to_fixed(q_val, *qmn_qi))
        i_val_qmn = str(float_to_fixed(i_val, *qmn_qi))
       
        # get a random position to check 
        pos = randint(0, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2]
        assert i_val_qmn == mem_values[pos*2+1]

    def test_tables_ff_qi(self, board, pref):
        """Test tables FF Q and I """
        qmn_qi = (1, 15, 1)
        q_val = set_table(pref, "FF", "SM", "Q", qmn_qi)
        i_val = set_table(pref, "FF", "SM", "I", qmn_qi)
    
        mem_pos = int(read_reg(board, FF_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        q_val_qmn = str(float_to_fixed(q_val, *qmn_qi))
        i_val_qmn = str(float_to_fixed(i_val, *qmn_qi))
       
        # get a random position to check 
        pos = randint(0, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2]
        assert i_val_qmn == mem_values[pos*2+1]
