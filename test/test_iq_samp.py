#!/usr/bin/env python3
import pytest
from random import randint, random

from helper import change_state, check_readback, float_to_fixed, fixed_to_float
from epics import caget, caput

class TestIQSampling:
    def test_mn_rbv(self, pref):
        # reset decimation to default values
        caput(pref + ":AI-DECF", 1)
        caput(pref + ":AI-DECO", 0)

        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)

        assert check_readback(pref + ":IQSMPL-NEARIQM", randint(0, 20))

        assert check_readback(pref + ":IQSMPL-NEARIQN", randint(0, 20))

    def test_cav_inp(self, pref):
        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)

        assert check_readback(pref + ":IQSMPL-CAVINDELAYEN", 1)
        assert check_readback(pref + ":IQSMPL-CAVINDELAYEN", 0)

        assert check_readback(pref + ":IQSMPL-CAVINDELAYVAL", randint(0, 63))

    def test_iq_ang_off(self, pref):
        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)

        assert check_readback(pref + ":IQSMPL-ANGOFFSETEN", 1)
        assert check_readback(pref + ":IQSMPL-ANGOFFSETEN", 0)

        mini = caget(pref + ":IQSMPL-ANGOFFSETVAL.DRVL")
        maxi = caget(pref + ":IQSMPL-ANGOFFSETVAL.DRVH")
        angoffset = round(random()*(maxi-mini)+mini, 16)
        caput(pref + ":IQSMPL-ANGOFFSETVAL", angoffset)

        res = caget(pref + ":IQSMPL-ANGOFFSETVAL-RBV")
        assert res == fixed_to_float(float_to_fixed(angoffset, 16, 16, 1), 16, 16, 1)
