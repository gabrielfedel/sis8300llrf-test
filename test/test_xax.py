#!/usr/bin/env python3
import pytest
from random import randint

from helper import change_state, sim_bp_trig
from epics import caget, caput


class TestXAxis:
    @pytest.mark.parametrize("ch", list(range(0,10)))
    def test_xax_ch(self, pref, ch):
        """ 
        Test X Axis for an specific channel
        """
        smnm = caget(pref + ":AI-SMNM-RBV")
        fac = caget(pref + (":AI%d-DECF-RBV") % ch)
        off = caget(pref + (":AI%d-DECO-RBV") % ch)
        f_samp = caget(pref + ":F-SAMPLING")

        # check first position
        vals = caget(pref + (":AI%d-XAX") % ch)
        assert '%.9f' % vals[0] == '%.9f' % (off * (1/(f_samp * 1000)))
      
        # check a random position
        maxi = (smnm//fac) - off - 1
        if maxi > 1 :
            pos = randint(1, maxi)
        else:
            pos = 1
    
        assert '%.6f' % vals[pos] == '%.6f' % ((pos*fac + off) * (1/(f_samp * 1000)))
