#!/usr/bin/env python3
import pytest
from random import randint

from helper import change_state, sim_bp_trig
from epics import caget, caput


class TestDecimation:
    def test_dec_rbv(self, pref):
        # enable all channels
        caput(pref + ":AI-ENBL-CH", 1)

        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)
        
        smnm = caget(pref + ":AI-SMNM-RBV")
        fac = randint(1, smnm//10)
        off = randint(0, int(smnm//fac - 1))

        caput(pref + ":AI-DECF", fac)
        caput(pref + ":AI-DECO", off)

        for c in range(0,10):
            fac_ch = caget(pref + (":AI%d-DECF-RBV") % c)
            off_ch = caget(pref + (":AI%d-DECO-RBV") % c)
            
            assert fac_ch == fac
            assert off_ch == off

    @pytest.mark.parametrize("ch", list(range(0,10)))
    def test_dec_ch(self, pref, ch):
        """ 
        Test decimation for an specific channel
        """
        smnm = caget(pref + ":AI-SMNM-RBV")
        fac = randint(1, smnm//10)
        off = randint(0, int(smnm//fac - 1))

        caput(pref + ":AI%d-DECF" % ch, fac)
        caput(pref + ":AI%d-DECO" % ch, off)

        fac_ch = caget(pref + (":AI%d-DECF-RBV") % ch)
        off_ch = caget(pref + (":AI%d-DECO-RBV") % ch)
        
        assert fac_ch == fac
        assert off_ch == off
   

    def test_ai_size(self, pref, board):
        """
        With decimation the size of AI waveform should change, this test
        check it. This test check the size of XAX too, that should be the
        same as AI waveform. This test should be runned after test_dec_rbv
        """
        assert change_state(pref, "ON")

        # run a pulse
        sim_bp_trig(board)
      
        smnm = caget(pref + ":AI-SMNM-RBV")

        for c in range (0,10):
            fac = caget(pref + (":AI%d-DECF-RBV") % c)
            off = caget(pref + (":AI%d-DECO-RBV") % c)

            nord = caget(pref + (":AI%d.NORD") % c)
            xax_nord = caget(pref + (":AI%d-XAX.NORD") % c)

            assert nord == int((smnm - off)/fac)
            assert xax_nord == int((smnm - off)/fac)
