import pytest
from helper import BASE_BOARD, SIS8300DRV_RTM_ATT_VM, get_mods_vers


def pytest_addoption(parser):
    parser.addoption("--board", required="True")
    parser.addoption("--pref", required="True")

def pytest_generate_tests(metafunc):
    if "board" in metafunc.fixturenames:
        b = metafunc.config.getoption("board")
        if b is not None:
            metafunc.parametrize("board", [BASE_BOARD + '-' + b])

    if "pref" in metafunc.fixturenames:
        p = metafunc.config.getoption("pref")
        if p is not None:
            metafunc.parametrize("pref", [p])

def pytest_report_header(config):
    return get_mods_vers()
