from random import random, randint
from math import pi, nan

from epics import caput, caget
from helper import change_state, float_to_fixed, read_reg, calc_lp_consts, \
                   calc_notch_consts, REG_LP1, REG_LP2, REG_LP3, REG_NOTCH1, \
                   REG_NOTCH2, REG_NOTCH3


class TestLowPass:
    def test_enable(self, board, pref):
        """Test Low Pass filter enable/disable"""
        caput(pref + ":CAVLPFIL-EN", 1)

        res = read_reg(board, REG_LP3)
        assert res == "0x1"

        caput(pref + ":CAVLPFIL-EN", 0)
        res = read_reg(board, REG_LP3)
        assert res == "0x0"

    def test_rand_values(self, board, pref):
        """Test Low Pass filter with random values"""
        cutoff = random()*10
        n = float(randint(1,10))

        fsamp = caget(pref + ":F-SAMPLING")

        caput(pref + ":CAVLPFIL-CUTOFF", cutoff)
        caput(pref + ":IQSMPL-NEARIQN", n)
        
        res = calc_lp_consts(cutoff, fsamp, n)

        a = float_to_fixed(res[0], 1, 15, 1)
        assert a != nan
        b = float_to_fixed(res[1], 1, 15, 1)
        assert b != nan
        
        a = hex(a)
        b = hex(b)
    
        reg1 = read_reg(board, REG_LP1)
        assert str(a) == reg1

        reg2 = read_reg(board, REG_LP2)
        assert str(b) == reg2


class TestNotch:
    def test_enable(self, board, pref):
        """Test Notch filter enable/disable"""
        caput(pref + ":NOTCHFIL-EN", 1)
        res = read_reg(board, REG_NOTCH3)

        assert res == "0x1"

        caput(pref + ":NOTCHFIL-EN", 0)
        res = read_reg(board, REG_NOTCH3)

        assert res == "0x0"

    def test_rand_values(self, board, pref):
        """Test Notch filter with random values"""
        freq = random()*10
        bwidth = random()*10
        n = float(randint(1,10))

        fsamp = caget(pref + ":F-SAMPLING")

        caput(pref + ":NOTCHFIL-FREQ", freq)
        caput(pref + ":NOTCHFIL-BWIDTH", bwidth)
        caput(pref + ":IQSMPL-NEARIQN", n)

        res = calc_notch_consts(bwidth, freq, fsamp, n)

        areal = float_to_fixed(res[0], 1, 15, 1)
        assert areal != nan
        aimag = float_to_fixed(res[1], 1, 15, 1)
        assert aimag != nan
        breal = float_to_fixed(res[2], 1, 15, 1)
        assert breal != nan
        bimag = float_to_fixed(res[3], 1, 15, 1)
        assert bimag != nan

        reg1exp = hex((areal << 16) | aimag)
        reg2exp = hex((breal << 16) | bimag)
    
        reg1 = read_reg(board, REG_NOTCH1)
        assert str(reg1exp) == reg1

        reg2 = read_reg(board, REG_NOTCH2)
        assert str(reg2exp) == reg2


