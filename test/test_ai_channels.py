#!/usr/bin/env python3
import pytest
from random import randint
from time import sleep

from helper import change_state, sim_bp_trig
from epics import caget, caput


class TestAIChannels:
    def test_en_all(self, pref):
        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)
             
        # enable all channels 
        for c in range(0,10): 
            caput(pref + (":AI%d-ENBL") % c, 1)
            sleep(1)
            res = caget(pref + (":AI%d-ENBL-RBV") % c)
            assert res == 1

            res = caget(pref + (":AI%d-ENBL-RBV.STAT") % c)
            sleep(1)
            assert res == 0

    @pytest.mark.parametrize("ch", list(range(0,10)))
    def test_acq(self, pref, board, ch):
        """
        Test to check if after a simulated pulse any data was acquired
        """
        # disable decimation
        caput(pref + ":AI-DECF", 1)
        caput(pref + ":AI-DECO", 0)
        # enable channel
        caput(pref + (":AI%d-ENBL") % ch, 1)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(pref, state)
        # run a pulse
        sim_bp_trig(board)


        smnm = caget(pref + ":AI-SMNM-RBV")

        # check if the number of acquired elements is equal to smnm
        ch_size = caget(pref + (":AI%d.NORD") % ch)
        assert smnm == ch_size

        # get a random position
        pos = randint(0, smnm-1)
        pos2nd = randint(0, smnm-1)
    
        vals = caget(pref + (":AI%d") % ch)

        # get read the value
        vals = caget(pref + (":AI%d") % ch)

        # at least one of 2 positions should be 0 (we expect that some
        # noise should be read)
        assert (vals[pos] != 0) or (vals[pos2nd] != 0)

    def test_dis_01(self, pref):
        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)
             
        # try to disable AI0 and AI1
        for c in range(0,2): 
            caput(pref + (":AI%d-ENBL") % c, 0)
            sleep(1)
            res = caget(pref + (":AI%d-ENBL-RBV") % c)
            assert res == 1


    def test_dis_29(self, pref):
        for state in ["RESET", "INIT"]:
            assert change_state(pref, state)

        # disable AI2-AI9 channels
        for c in range(2,10): 
            caput(pref + (":AI%d-ENBL") % c, 0)
            sleep(1)
            res = caget(pref + (":AI%d-ENBL-RBV") % c)
            assert res == 0

            res = caget(pref + (":AI%d-ENBL-RBV.STAT") % c)
            assert res == 0
