#!/usr/bin/python
import sys
import numpy
import epics
if (len(sys.argv) < 3 or (len(sys.argv) == 2 and (sys.argv[1] == '--h' or sys.argv[1] == '-help'))):
	print '\nUSAGE: sis8300llrf-demo-readTable.py [pv_name] [filename]\n'
	print 'ARGUMENTS:'
	print '\t[pv_name]  table to read'
	print '\t[filename] file to export the data'
	print 'ACTION:'
	print '\tThis will load the table from [pv_name] and write it to [filename]'
	sys.exit(-1)
p = epics.PV(sys.argv[1])
#these tables need to be processed
(epics.PV(sys.argv[1]+".PROC")).put(1)
wf_data = p.get();
numpy.savetxt(sys.argv[2], wf_data, delimiter='\n')
