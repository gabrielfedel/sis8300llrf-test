/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfDevice.cpp
 * @brief Implementation of LLRF device in NDS.
 * @author urojec
 * @date 30.5.2014
 */
#include <unistd.h>

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfAIChannel.h"
#include "sis8300llrfChannel.h"
#include "sis8300llrfPIChannel.h"
#include "sis8300llrfIQSamplingChannel.h"
#include "sis8300llrfVMControlChannel.h"
#include "sis8300llrfControlTableChannel.h"
#include "sis8300llrfModRippleFiltChannel.h"
#include "sis8300llrfNotchFilterChannel.h"
#include "sis8300llrfLowPassFiltChannel.h"
#include "sis8300llrfILOCKChannel.h"
#include "sis8300RegisterChannel.h"
#include "sis8300llrfSignalMonitorChannel.h"

#include "sis8300llrfDevice.h"

nds::RegisterDriver<sis8300llrfDevice> exportedDevice("sis8300llrf");

std::string sis8300llrfDevice::PV_REASON_FORCE_TRIGG 		  = "ForceTrigger";
std::string sis8300llrfDevice::PV_REASON_OPERATING_MODE       = "OperatingMode";
std::string sis8300llrfDevice::PV_REASON_PULSE_TYPE           = "PulseType";
std::string sis8300llrfDevice::PV_REASON_PULSE_DONE_COUNT     = "PulseDoneCount";
std::string sis8300llrfDevice::PV_REASON_PULSE_MISSED         = "PulseMissed";
std::string sis8300llrfDevice::PV_REASON_UPDATE_REASON        = "UpdateReason";
std::string sis8300llrfDevice::PV_REASON_PMS_ACT              = "PmsAct";
std::string sis8300llrfDevice::PV_REASON_ARM                  = "Arm";
std::string sis8300llrfDevice::PV_REASON_PULSE_DONE           = "PulseDone";
std::string sis8300llrfDevice::PV_REASON_STATUS               = "Status";
std::string sis8300llrfDevice::PV_REASON_SETUP_ACTIVE         = "SetupActive";
std::string sis8300llrfDevice::PV_REASON_SIGNAL_ACTIVE        = "SignalActive";
std::string sis8300llrfDevice::PV_REASON_F_SAMPLING           = "FSampling";

/**
 * @brief Device constructor.
 * @param [in] name Device name.
 *
 * LLRF device constructor. For details refer to NDS documentation.
 */
sis8300llrfDevice::sis8300llrfDevice(const std::string& name) :
        sis8300Device(name) {


    registerOnEnterStateHandler(nds::DEVICE_STATE_ERROR,
            boost::bind(&sis8300llrfDevice::onEnterError, this, _1, _2));

    registerOnRequestStateHandler(nds::DEVICE_STATE_INIT, nds::DEVICE_STATE_ON,
            boost::bind(&sis8300llrfDevice::onSwitchOn, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_ON,
            boost::bind(&sis8300llrfDevice::onEnterOn, this, _1, _2));
    registerOnLeaveStateHandler(nds::DEVICE_STATE_ON,
            boost::bind(&sis8300llrfDevice::onLeaveOn, this, _1, _2));

    /* Do similar for reset as it is done for processing -> first call
     * the device onEnterReset and than send all CGs to resetting
     *
     * order of registration IS IMPORTAINT!
     */
    registerOnEnterStateHandler(nds::DEVICE_STATE_RESETTING,
            boost::bind(&sis8300llrfDevice::onEnterReset, this, _1, _2));

    registerOnEnterStateHandler(nds::DEVICE_STATE_RESETTING,
            boost::bind(&sis8300llrfDevice::llrfCgStateSet, this, nds::CHANNEL_STATE_RESETTING));


    /* add check for PMS to all request transitions
     * (request INIT and request ON)
     */
    registerOnRequestStateHandler(nds::DEVICE_STATE_RESETTING, nds::DEVICE_STATE_INIT,
            boost::bind(&sis8300llrfDevice::checkPms, this));
    registerOnRequestStateHandler(nds::DEVICE_STATE_OFF, nds::DEVICE_STATE_INIT,
                boost::bind(&sis8300llrfDevice::checkPms, this));
    registerOnRequestStateHandler(nds::DEVICE_STATE_INIT, nds::DEVICE_STATE_ON,
            boost::bind(&sis8300llrfDevice::checkPms, this));


    prohibitTransition(nds::DEVICE_STATE_ON, nds::DEVICE_STATE_INIT);
    prohibitTransition(nds::DEVICE_STATE_ON, nds::DEVICE_STATE_OFF);
    prohibitTransition(nds::DEVICE_STATE_OFF, nds::DEVICE_STATE_RESETTING);
    prohibitTransition(nds::DEVICE_STATE_RESETTING, nds::DEVICE_STATE_ON);

    /* handle INIT msg */
    registerMessageWriteHandler("INIT",
            boost::bind(&sis8300llrfDevice::handleInitMsg, this, _1, _2));

    /* Main task taking care of the loop when controller is on
         */
    _ControlLoopTask = nds::ThreadTask::create(
            nds::TaskManager::generateName("controlLoopTask"),
            epicsThreadGetStackSize(epicsThreadStackMedium),
            epicsThreadPriorityHigh,
            boost::bind(&sis8300llrfDevice::controlLoopTask, this, _1));

    _PulseType = 0;
    _PulseTypeChanged = 0;

    _DeviceUser = NULL;
    _PMSAct = 0;

    _OperatingMode = mode_normal;
    
    _UpdateReason = 0;

}

/**
 * @brief LLRF device destructor. For details refer to NDS documentation.
 * 
 * @param [in] name Device name.
 *
 */
sis8300llrfDevice::~sis8300llrfDevice() {
    
    if (_DeviceUser) {
        sis8300llrfdrv_sw_reset(_DeviceUser);
    }
}

/**
 * @brief Create driver structures.
 *
 * @param [in] portName AsynPort name.
 * @param [in] params   Refer to NDS documentation.
 *
 * @retval ndsSuccess createStructure always returns success.
 *
 * The function creates all the channel groups and registers channels with them.
 * This is achieved by calling the following functions:
 *  - #registerAIChannels
 *  - #registerControlTableChannels
 *  - #registerControllerChannels
 *  - #registerSignalMonitorChannels
 *  - #registerRegisterChannels
 *
 * Because we require custom channel groups and channels we override the createStructure
 * from base class. We also don't register trigger channel groups and AO channel groups
 *
 * This function also allocates space for #_DeviceUser and copies the value of the
 * FILE parameter into it.
 */
ndsStatus sis8300llrfDevice::createStructure(const char* portName, const char* params) {
    NDS_TRC("%s", __func__);
    
    ndsStatus status;
    nds::ChannelGroup *cg;

    _MaxPulseType = (epicsInt32) (getIntParam("NUM_PULSE_TYPES", 0) - 1);
    if (_MaxPulseType < 0) {
        NDS_CRT("Number of pulse types requested is <= 0. Cannot continue");
        return ndsError;
    }

    _DeviceUser = (sis8300drv_usr *) calloc(1, sizeof(sis8300drv_usr));
    _DeviceUser->file = getStrParam("FILE", "").c_str();

    _FSampling = (epicsFloat64) atof((getStrParam("FSAMPLING", "0")).c_str());
    if (_FSampling == 0) 
        _FSampling = FSAMPLING_DEFAULT;
    NDS_DBG("F Sampling %f", _FSampling);


    /* this one has to be the first, so that the asyn addr matches the one in parent */
    status = registerAIChannels();
    if (status != ndsSuccess) {
        return status;
    }

    status = registerControllerChannels();
    if (status != ndsSuccess) {
        return status;
    }

    status = registerControlTableChannels(ctrl_table_sp, _MaxPulseType + 1);
    if (status != ndsSuccess) {
        return status;
    }

    status = registerControlTableChannels(ctrl_table_ff, _MaxPulseType + 1);
    if (status != ndsSuccess) {
        return status;
    }

    status = registerSignalMonitorChannels();
    if (status != ndsSuccess) {
        return status;
    }

    status = registerRegisterChannels();
    if (status != ndsSuccess) {
        NDS_ERR("Failed to register Register Channels");
    }
    else {
        getChannelGroup(SIS8300NDS_REGCG_NAME, &cg);
        _CgReg = dynamic_cast<sis8300RegisterChannelGroup *>(cg);
    }
    
    /* in the same order that they are registered - matches asyn addr*/
     _llrfChannelGroups[0] = _CgCtrl;
     _llrfChannelGroups[1] = _CgSP;
     _llrfChannelGroups[2] = _CgFF;
     _llrfChannelGroups[3] = _CgSigmon;

    return ndsSuccess;
}

/**
 * @brief Register analog input channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registers a #sis8300llrfAIChannelGroup named #SIS8300NDS_AICG_NAME
 * and then registers #SIS8300DRV_NUM_AI_CHANNELS times #sis8300AIChannel
 * with the channel group.
 */
ndsStatus sis8300llrfDevice::registerAIChannels() {
    int iter;
    ndsStatus status;

    _CgAI = new sis8300llrfAIChannelGroup(SIS8300NDS_AICG_NAME);
    status = registerChannelGroup(_CgAI);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300NDS_AICG_NAME);
        return ndsError;
    }

    for (iter = 0; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {

        if (iter == SIS8300LLRFDRV_AI_CHAN_CAV || iter == SIS8300LLRFDRV_AI_CHAN_REF) {
            status = _CgAI->registerChannel(new sis8300llrfAIChannel(_FSampling));
        }
        else {
            status = _CgAI->registerChannel(new sis8300AIChannel(_FSampling));
        }

        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", SIS8300NDS_AICG_NAME, iter);
            return ndsError;
        }
    }

    _CgAI->setDeviceUser(_DeviceUser);

    return ndsSuccess;
}

/**
 * @brief Register controller channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registers a #sis8300llrfControllerChannelGroup named
 * #SIS8300LLRFNDS_CONTROLLER_CG_NAME and then registers
 * two #sis8300llrfPIChannel and two #sis8300llrfIOControlChannel
 * channels
 */
ndsStatus sis8300llrfDevice::registerControllerChannels() {
    ndsStatus status;
    int i;

    _CgCtrl = new sis8300llrfControllerChannelGroup(SIS8300LLRFNDS_CONTROLLER_CG_NAME, _DeviceUser);
    status = registerChannelGroup(_CgCtrl);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    /* register all controler channels. Order is importaint, @see
     * #SIS8300LLRFNDS_PI_I_CHAN_ADDR, #SIS8300LLRFNDS_PI_Q_CHAN_ADDR,
     * #SIS8300LLRFNDS_IO_IQ_CHAN_ADDR, #SIS8300LLRFNDS_IO_VM_CHAN_ADDR
     */

    status = _CgCtrl->registerChannel(new sis8300llrfPIChannel(pi_I));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel PI magnitude.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfPIChannel(pi_Q));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel PI angle.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfIQSamplingChannel());
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel IQ control.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfVMChannel());
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel VM control.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfModRippleFiltChannel());
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel modulator ripple filter setup.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    /* Register 4 channels for interlock */
    for (i = 0; i < SIS8300DRV_NUM_HAR_CHANNELS; i++) {
        status = _CgCtrl->registerChannel(new sis8300llrfILOCKChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel ILOCK %i.",
                    SIS8300LLRFNDS_CONTROLLER_CG_NAME, i);
            return ndsError;
        }
    }
    
    status = _CgCtrl->registerChannel(new sis8300llrfNotchFilterChannel(_FSampling));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel notch filter setup.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfLowPassFiltChannel(_FSampling));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel low pass filter setup.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }


    return ndsSuccess;
}

/**
 * @brief Register control table channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registeres a #sis8300llrfControlTableChannelGroup named
 * #SIS8300LLRFNDS_SPCG_NAME or #SIS8300LLRFNDS_FFCG_NAME
 * and then registeres #pulseTypes times #sis8300llrfControlTableChannel
 * with the channel group.
 */
ndsStatus sis8300llrfDevice::registerControlTableChannels(sis8300llrfdrv_ctrl_table ctrlTableType, int pulseTypes) {
    int iter;
    ndsStatus status;
    sis8300llrfControlTableChannelGroup *cg;

    switch (ctrlTableType) {
        case ctrl_table_sp:
            _CgSP = new sis8300llrfControlTableChannelGroup(SIS8300LLRFNDS_SPCG_NAME, _DeviceUser, ctrlTableType);
            cg = _CgSP;
            break;
        case ctrl_table_ff:
            _CgFF = new sis8300llrfControlTableChannelGroup(SIS8300LLRFNDS_FFCG_NAME, _DeviceUser, ctrlTableType);
            cg = _CgFF;
            break;
        default:
            NDS_ERR("Create ControlTableChannelGroup called with wrong control table type.");
            return ndsError;
    }

    status = registerChannelGroup(cg);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", (cg->getName()).c_str());
        return ndsError;
    }

    for (iter = 0; iter < pulseTypes; iter++) {
        status = cg->registerChannel(new sis8300llrfControlTableChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel(pulse type) %d.",
                    (cg->getName()).c_str(), iter);
            return ndsError;
        }
    }

    return ndsSuccess;
}

/**
 * @brief Register signal monitoring channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registers a #sis8300llrfChannelGroup named #SIS8300LLRFNDS_SIGMON_CG_NAME
 * and then registers #SIS8300DRV_NUM_AI_CHANNELS times #sis8300llrfSigmonChannel
 * with the channel group.
 */
ndsStatus sis8300llrfDevice::registerSignalMonitorChannels() {
    int iter;
    ndsStatus status;

    _CgSigmon = new sis8300llrfChannelGroup(SIS8300LLRFNDS_SIGMON_CG_NAME, _DeviceUser);
    
    if (registerChannelGroup(_CgSigmon) != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300LLRFNDS_SIGMON_CG_NAME);
        return ndsError;
    }
    
    for (iter = 0; iter < SIS8300LLRFDRV_SIGMON_CHAN_FIRST; iter++) {
        status = _CgSigmon->registerChannel(new sis8300llrfChannel(0, 0));
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", SIS8300LLRFNDS_SIGMON_CG_NAME, iter);
            return ndsError;
        }
    }

    for (; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {
        status = _CgSigmon->registerChannel(new sis8300llrfSignalMonitorChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", SIS8300LLRFNDS_SIGMON_CG_NAME, iter);
            return ndsError;
        }
    }

    return ndsSuccess;
}

/**
 * @brief Check PMS state and send the device to error if it is active
 *
 * @return ndsError   pms is active
 * @return ndsSuccess pms not active
 *
 * This is a state transition handler called when INIT or ON state is
 * requested and prevents the transition if PMS is active
 */
ndsStatus sis8300llrfDevice::checkPms() {
    int status;

    status = sis8300llrfdrv_get_general_status(_DeviceUser, gen_status_PMS_active, &_PMSAct);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_general_status", status);

    doCallbacksInt32((epicsInt32)_PMSAct, _interruptIdPmsAct);

    if (_PMSAct) {
        error();
        return ndsError;
    }

    return ndsSuccess;
}

/**
 * @brief Check Signal Status (if it is active)
 *
 * This is used for determining the signal state during Special operating mode
 * 
 * @warning This should NOT be used in normal operation mode, this information 
 * is only correct when the controller is used in continuous wave mode.
 * In any other case it may be misleading
 */
inline ndsStatus sis8300llrfDevice::checkSignalStatus() {
    NDS_TRC("%s", __func__);
    
    int status;
    unsigned uVal;

    status = sis8300llrfdrv_get_signal_active(_DeviceUser, &uVal);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_signal_active", status);

    doCallbacksInt32((epicsInt32) uVal, _interruptIdSignalActive);
    
    return ndsSuccess;
}

/**
 * @brief State handler for requesting the INIT state.
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError Failed to open device or read info.
 *
 * Calls the parents onSwitchInit handler and reads basic info,
 * LLRF specific information from the device. It also checks if the libray
 * and firmware versions match and refuses the transition on mismatch.
 */
ndsStatus sis8300llrfDevice::onSwitchInit(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    int status;
    char buffer[SIS8300NDS_STRING_BUFFER_SIZE];
    unsigned fwDevice, fwMajor, fwMinor;

    status = sis8300Device::onSwitchInit(from, to);
    if ((ndsStatus) status != ndsSuccess) {
        return ndsBlockTransition;
    }

    status = sis8300llrfdrv_get_fw_version(_DeviceUser, &fwDevice, &fwMajor, &fwMinor);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_fw_version", status);
    NDS_INF("sis8300llrfdrv_get_fw_version returned version V:%#04x.%#04x, Device: %#06x",
            fwMajor, fwMinor, fwDevice);

    if (fwDevice != SIS8300LLRFDRV_HW_ID) {
        NDS_ERR("Card hardware id: %#06x not compatible with library hardware id: %#06x",
                fwDevice, SIS8300LLRFDRV_HW_ID);
        SIS8300NDS_MSGERR("Hardware id not compatible with library hw id");
        return ndsBlockTransition;
    }
    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE  - 1, "%#06x", fwDevice);
    _model = buffer;
    doCallbacksOctetStr(_model, ASYN_EOM_END, _interruptIdModel, _portAddr);
    
    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%u.%02u", fwMajor, fwMinor);
    _firmwareVersion = buffer;
    doCallbacksOctetStr(_firmwareVersion, ASYN_EOM_END, _interruptIdFirmware);
        
    /* Check firmware version is supported by the software */
    status = check_fw_version(fwMajor, fwMinor);
    if (status != ndsSuccess) {    
        NDS_CRT("Firmware version unsupported: Version %d.%d is not currently supported.", fwMajor, fwMinor);
        return ndsError;
    }
    SIS8300NDS_STATUS_ASSERT("sis8300llrfDevice::check_fw_version", status);
    /* TODO: sw id? */

    /* TODO: remove, this is a workaround */
    _CgReg->setDeviceUser(_DeviceUser);

    return ndsSuccess;
}

/**
 * @brief State handler for entering INIT state.
 *
 * @param [in] from Source state.
 * @param [in] to   Destination state.
 *
 * @return ndsError   Setup failed
 * @return ndsSuccess Device successfully initialized.
 *
 * The function prevents the parnet handler to initialize the ADC and DAC!
 * Than does LLRF specific setup. The setup depends on which state we arrived
 * from. If we did not arrive from OFF or RESETTING than an error is returned.
 */
ndsStatus sis8300llrfDevice::onEnterInit(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    int status, i;
    epicsInt32  samplesCountMax;
    ndsStatus   statusSamplesCount;

    unsigned fwDevice, fwMajor, fwMinor;
    //firmware version used to determine if certain registers are required for operation 
    status = sis8300llrfdrv_get_fw_version(_DeviceUser, &fwDevice, &fwMajor, &fwMinor);
	
    
    /* To be sure of the device state, does not hurt */
    status = sis8300llrfdrv_sw_reset(_DeviceUser);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_sw_reset", status);

    /* Debugging and logging */
    _CgReg->readAllChannels();

    /* Initialize the board */
    status = sis8300llrfdrv_mem_ctrl_set_custom_mem_map(_DeviceUser, (unsigned) _MaxPulseType + 2);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_mem_ctrl_set_custom_mem_map", status);
    
    status = sis8300drv_init_adc(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_init_adc", status);

    status = sis8300llrfdrv_setup_dac(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_setup_dac", status);

    if (fwMajor == SIS8300LLRFDRV_VERSION_MAJOR_KU) { // KU board, register "adc_tap_delay not used".
      status = ndsSuccess;
    }
    else { // Older board versions, L2 etc
      status = sis8300llrfdrv_setup_adc_tap_delay(_DeviceUser);
      SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_setup_adc_tap_delay", status);
    } 
    
    /* Initialize the generic NDS part
     * parent onEnterInit handler does this. But it also initializes the DAC and ADC and
     * we want to prevent this here. So we copy this part of AI channel group initialization
     * from the parent */
    if (_CgAI != NULL) {
        NDS_DBG("Initializing channel group %s.", _CgAI->getName().c_str());
        statusSamplesCount = _CgAI->checkSamplesConfig(-1, -1, &samplesCountMax);
        if (statusSamplesCount != ndsSuccess) {
            _CgAI->setSamplesCount(NULL, samplesCountMax);
        }
        _CgAI->markAllParametersChanged();
        _CgAI->commitParameters();
        _CgAI->commitAllChannels();
    }

    /* Initialize the llrf NDS part */
    if (from == nds::DEVICE_STATE_OFF) {
        
        for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
            if (_llrfChannelGroups[i]->initialize() != ndsSuccess) {
                NDS_ERR("Could not initialize CG %s", _llrfChannelGroups[i]->getName().c_str());
                SIS8300NDS_MSGERR("Could not initialize CG");
                error();
                return ndsError;
            }
            else {
                NDS_INF("Sucessfully inititalized CG %s", _llrfChannelGroups[i]->getName().c_str());
            }
        }

        /* Default operating mode is normal */
        setOperatingMode(NULL, (epicsInt32) mode_normal);
        /* Set pulse type when going to init */
        setPulseType(NULL, _PulseType);
    }
    else if (from == nds::DEVICE_STATE_RESETTING) {

        /* When CGs and CHs enter the disabled state they will rewrite all
         * the values to the controller */
        llrfCgStateSet(nds::CHANNEL_STATE_DISABLED);
        
        /* Rewrite the device paramters to the controller */
        setOperatingMode(NULL, (epicsInt32) _OperatingMode);
        setPulseType(NULL, _PulseType);

    }
    else {
        NDS_ERR("Arriving to INIT from an unsupported state, %i", (int) from);
        SIS8300NDS_MSGERR("Arriving to INIT from an unsupported state");
        error();
        return ndsError;
    }

    NDS_INF("%s is in %s.", _ControlLoopTask->getName().c_str(), _ControlLoopTask->getStateStr().c_str());

    return ndsSuccess;
}

/**
 * @brief Override fast init
 *
 * @param [in] from ignored
 * @param [in] to   ignored
 *
 * @return ndsSuccess always
 */
ndsStatus sis8300llrfDevice::fastInit(nds::DeviceStates from, nds::DeviceStates to) {
    return ndsSuccess;
}

/**
 * @brief State handler for request INIT->ON request
 *
 * @param [in] from Source state.
 * @param [in] to   Destination state.
 *
 * @retval ndsSuccess         Transition allowed.
 * @retval ndsBlockTransition Parameters are not ok
 *
 * Checks if the control tables for the selected pulse type are ok.
 * If not, the transition is blocked
 */
ndsStatus sis8300llrfDevice::onSwitchOn(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    if (_SetupActive) {
        NDS_ERR("Calibration must be done in INIT state");
        SIS8300NDS_MSGERR("Calibration must be done in INIT state");
        return ndsBlockTransition;
    }
    if (_OperatingMode == mode_normal) {
        if (_PulseType < 0) {
            NDS_WRN("No pulse type selected! Cannot switch on!");
            SIS8300NDS_MSGERR("No pulse type selected! Cannot switch on!");
            return ndsBlockTransition;
        }

        if (setPulseType(NULL, _PulseType) != ndsSuccess) {
            NDS_ERR("Pulse type not set up, denying switch ON");
            SIS8300NDS_MSGERR("Pulse type not set up, denying request to switch ON");
            return ndsBlockTransition;
        }
    }
    else {
        if (setPulseType(NULL, _MaxPulseType + 1) != ndsSuccess) {
            NDS_WRN("Tables for Spec mode are not set up, deny switch ON");
            SIS8300NDS_MSGERR("Tables for Spec mode are not set up, denying request to switch ON");
            return ndsBlockTransition;
        }
    }

    return ndsSuccess;
}


/**
 * @brief state handler for transition to ON
 *
 * @param [in]  from    Source state
 * @param [in]  to      Destination state (ON)
 *
 * @retval ndsSuccess   Transition successfull
 * @retval ndsError     Something failed
 *
 * This will also automatically start all CGs groups so no need to do it
 * separately. 
 * The function will set the init done bit on the device which will start the
 * control loop on the device. If anything fails, the device will move to error state.
 */
ndsStatus sis8300llrfDevice::onEnterOn(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    int status;

    status = sis8300llrfdrv_init_done(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_init_done", status);
    
    _UpdateReason = (epicsInt32) SIS8300LLRFDRV_UPDATE_REASON_INIT_DONE;
    doCallbacksInt32(_UpdateReason , _interruptIdUpdateReason);
    _PulseTypeChanged = 0;

    _CgReg->readAllChannels();

    _ControlLoopTask->start();
    NDS_INF("%s is in %s.", _ControlLoopTask->getName().c_str(), _ControlLoopTask->getStateStr().c_str());

    return ndsSuccess;
}

/**
 * @brief State handler for transition out of ON.
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Always.
 *
 * Parent closes the device, since it does not have the INIT state.
 * We don't want to do this.
 *
 * Similar to switch on, parent's onLeaveOn translates to this
 * onEnterOff with from == INIT
 */
ndsStatus sis8300llrfDevice::onLeaveOn(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);
    
    _CgReg->readAllChannels();

    /* TODO: start waiting for PMS? - no we check for this on all state transitions
     * so it is ok. Only in ON do we need to consantly have this monitored */

    return ndsSuccess;
}


ndsStatus sis8300llrfDevice::onEnterOffState(nds::DeviceStates from, nds::DeviceStates to) {

    llrfCgStateSet(nds::CHANNEL_STATE_DISABLED);

    return sis8300Device::onEnterOffState(from, to);
}


/**
 * @brief State handler for entering the RESET state
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError   Problem accessing device
 *
 * The function releases all waiters for the interrupt and
 * requests a sw reset of custom logic. If OFF state was requested,
 * it requests the transition of the device to OFF.
 */
ndsStatus sis8300llrfDevice::onEnterReset(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);
    
    int status;

    /* Release waiters - _PulseSetupTask */
    status = sis8300drv_release_irq(_DeviceUser, irq_type_usr);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_release_irq", status);

    /* Wait for _ControlLoopTaslk to terminate */
    usleep(1);

    status = sis8300llrfdrv_sw_reset(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_sw_reset", status);


    /* Check what happened to PMS active state, it should have gone away */
    status = sis8300llrfdrv_get_general_status(_DeviceUser, gen_status_PMS_active, &_PMSAct);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_general_status", status);
    doCallbacksInt32((epicsInt32)_PMSAct, _interruptIdPmsAct);
    if (_PMSAct) {
        NDS_ERR("PMS not cleared on RESET!");
        SIS8300NDS_MSGERR("PMS not cleared on RESET!");
        error();
        return ndsError;
    } 

    doCallbacksInt32((epicsInt32) SIS8300LLRFNDS_STATUS_CLEAR, _interruptIdStatus);
    
    NDS_INF("%s is in %s.", _ControlLoopTask->getName().c_str(), _ControlLoopTask->getStateStr().c_str());

    _CgAI->off();
    
    // Reset Pulse Done Counter
    // No need to read back from hardware as device pulse count is incremental not absolute
    _PulseDoneCount = 0;
    doCallbacksInt32((epicsInt32)_PulseDoneCount, _interruptIdPulseDoneCount); 
    return checkSignalStatus();
}

/**
 * @brief State handler for entering the ERROR state
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Always.
 *
 * Parent closes the device, but parent does not have INIT state. Here, we have a
 * possibility of a sw reset, which means that we will leave the on state, but will
 * not want to close the device
 *
 * Similar to switch on, parent's onLeaveOn translates to this onEnterOff with from == INIT
 */
ndsStatus sis8300llrfDevice::onEnterError(nds::DeviceStates from, nds::DeviceStates to) {

    unsigned status;

    /* Release waiters - _PulseSetupTask */
    status = sis8300drv_release_irq(_DeviceUser, irq_type_usr);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_release_irq", status);

    /* Check for PMS */
    status = sis8300llrfdrv_get_general_status(_DeviceUser, gen_status_PMS_active, &_PMSAct);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_general_status", status);
    if(_PMSAct) {
        doCallbacksInt32((epicsInt32) _PMSAct, _interruptIdPmsAct);
        doCallbacksInt32((epicsInt32) SIS8300LLRFNDS_STATUS_PMS, _interruptIdStatus);
    }
    
    /* stop all CGs and CHs */
    stopAllGroups();

    NDS_INF("Pulse setup task is in: %s", _ControlLoopTask->getStateStr().c_str());

    return ndsSuccess;
}

/**
 * @brief Message handler for RESET state
 *
 * @param [in] pasynUser  Asyn user
 * @param [in] value      Message value
 *
 * @return ndsSuccess always
 *
 * The function directly sets the RESET state
 */
ndsStatus sis8300llrfDevice::handleResetMsg(asynUser *pasynUser, const nds::Message& value) {

    NDS_TRC("%s", __func__);

    if (getCurrentState() == nds::DEVICE_STATE_OFF) {
        NDS_ERR("Cannot set RESET in OFF state");
        SIS8300NDS_MSGERR("Cannot request RESET in OFF state");
        return ndsBlockTransition;
    }

    getCurrentStateObj()->setMachineState(this, nds::DEVICE_STATE_RESETTING);

    return ndsSuccess;
}

/**
 * @brief Message handler for OFF state
 *
 * @param [in] pasynUser  Asyn user
 * @param [in] value      Message value
 *
 * @return ndsSuccess Always
 *
 * If the device is in ON state, which means the custom logic is busy,
 * the device is sent to RESETTING and #_OffRequested flag goes high,
 * which ensures that the device will go to OFF after reset. If the
 * device is not on, it goes directly to OFF.
 */
ndsStatus sis8300llrfDevice::handleOffMsg(asynUser *pasynUser, const nds::Message& value) {

    NDS_TRC("%s", __func__);

    if (getCurrentState() == nds::DEVICE_STATE_ON) {
        SIS8300NDS_MSGERR("OFF cannot be requested from ON");
        NDS_ERR("OFF cannot be requested from ON");
        return ndsBlockTransition;
    }
    
    getCurrentStateObj()->requestState(this, nds::DEVICE_STATE_OFF);

    return ndsSuccess;
}

/**
 * @brief message handler for INIT msg
 *
 * @param [in] pasynUser  Asyn user
 * @param [in] value      Message value
 *
 * @return ndsSuccess         Transition allowed
 * @return ndsBlockTransition Transition not allowed
 *
 * Transition to INIT is only allowed from OFF or
 * RESETTING
 */
ndsStatus sis8300llrfDevice::handleInitMsg(asynUser *pasynUser, const nds::Message& value) {

    NDS_TRC("%s", __func__);

    if (getCurrentState() != nds::DEVICE_STATE_OFF &&
        getCurrentState() != nds::DEVICE_STATE_RESETTING) {
        SIS8300NDS_MSGERR("INIT state can be requested only from OFF or RESETTING");
        NDS_ERR("INIT state can be requested only from OFF or RESETTING");
        return ndsBlockTransition;
    }

    getCurrentStateObj()->requestState(this, nds::DEVICE_STATE_INIT);

    return ndsSuccess;
}

/**
 * @brief Override parent because it overrides our messages
 */
ndsStatus sis8300llrfDevice::handleOnMsg(asynUser *pasynUser, const nds::Message& value) {

    if (getCurrentState() != nds::DEVICE_STATE_INIT) {
        SIS8300NDS_MSGERR("ON state can only be requested from INIT");
        NDS_ERR("ON state can only be requested from INIT");
        return ndsBlockTransition;
    }

    return on();
}

/**
 * @brief task that waits for PULSE_DONE interrupt and setsup the new pulse parameters
 *
 *
 * This task is continuously running when the sis8300llrfDevice is in on state.
 * This is because during the on state, the controller is in action. After passing
 * of each pulse, the controller emits a PULSE_DONE interrupt. The task catches the
 * interrupt upon which it
 *
 * 1. Checks if the device is on (meaning the custom logic is running) and
 *    terminates if not
 * 2. Sends all the channel groups into disabled state
 * 3. Checks if new pulse type was requested
 * 4. Determines update reason and sends it to hw
 * 5. Sends all the channel groups to processing state
 *
 */
ndsStatus sis8300llrfDevice::controlLoopTask(nds::TaskServiceBase &service) {
    NDS_TRC("%s", __func__);

    unsigned pulseCntDevice;
    int status, irq_status, i;

    _PulseDoneCount = 0;

    while (getCurrentState() == nds::DEVICE_STATE_ON) {
/*****
 * LOCKING THE DEVICE AND CG PORT!!!
 *****/
        lock();
        status = sis8300llrfdrv_clear_latched_statuses(_DeviceUser, SIS8300LLRFDRV_STATUS_CLR_GENERAL | SIS8300LLRFDRV_STATUS_CLR_SIGMON);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_clear_gop", status, this);

        logGenericPredefinedRegisters();
        
        status = sis8300llrfdrv_get_general_status(_DeviceUser, gen_status_PMS_active, &_PMSAct);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_get_general_status", status, this);
        if(_PMSAct) {
            NDS_ERR("PMS Active");
            doCallbacksInt32((epicsInt32)_PMSAct, _interruptIdPmsAct);
            error();
            unlock();
            return ndsSuccess;
        }

        /* We clear this every time an interrupt is recieved, so that we 
         * can raise an alarm if we are missing pulses */
        status = sis8300llrfdrv_clear_pulse_done_count(_DeviceUser);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_clear_pulse_done_count", status, this);
        
        if (_OperatingMode == mode_normal) {
            status = sis8300llrfdrv_arm_device(_DeviceUser);
            SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_arm_device", status, this);
        }
        else {
            status = sis8300llrfdrv_arm_device_unlocked(_DeviceUser);
            SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_arm_device_unlocked", status, this);
        }

        doCallbacksInt32((epicsInt32) 1, _interruptIdArm);
        doCallbacksInt32((epicsInt32) SIS8300LLRFNDS_STATUS_ARMED, _interruptIdStatus);

        unlock();
/*****
 * UNLOCKING THE DEVICE AND CG PORT TO WAIT FOR IRQ
 *****/
        irq_status = sis8300llrfdrv_wait_pulse_done_pms(_DeviceUser, SIS8300LLRFNDS_IRQ_WAIT_TIME);

        if (irq_status == status_irq_release) {
            if (getCurrentState() != nds::DEVICE_STATE_RESETTING) {
                NDS_ERR("Unknown source released waiting for PULSE DONE");
                SIS8300NDS_MSGERR("Unknown source released waiting for PULSE DONE");
                error();
            }
            return ndsSuccess;
        }
        else {
            SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_wait_pulse_done_pms", irq_status);
        }
/*****
 * LOCKING THE DEVICE AND CG PORT!!!
 *****/
        lock();

        status = sis8300llrfdrv_get_general_status(_DeviceUser, gen_status_PMS_active, &_PMSAct);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_get_general_status", status, this);
        if(_PMSAct) {
            doCallbacksInt32((epicsInt32) _PMSAct, _interruptIdPmsAct);
            doCallbacksInt32((epicsInt32) SIS8300LLRFNDS_STATUS_PMS, _interruptIdStatus);
            NDS_ERR("PMS Active");
            error();
            unlock();
            return ndsSuccess;
        } else {
            doCallbacksInt32((epicsInt32) SIS8300LLRFNDS_STATUS_PULSE_DONE, _interruptIdStatus);
            doCallbacksInt32((epicsInt32) 1, _interruptIdPulseDone);
        }
        
        logGenericPredefinedRegisters();

        if (stopAllGroups() != ndsSuccess) {
            error();
            unlock();
            return ndsSuccess;
        }

        status = sis8300llrfdrv_get_pulse_done_count(_DeviceUser, &pulseCntDevice);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_get_pulse_done_count", status, this);

        //THINK: go to error on missed pulse?
        doCallbacksInt32((epicsInt32)pulseCntDevice - 1, _interruptIdPulseMissed);
        _PulseDoneCount += pulseCntDevice;
        doCallbacksInt32((epicsInt32)_PulseDoneCount, _interruptIdPulseDoneCount);
 
        status = sis8300llrfdrv_get_general_status(_DeviceUser, gen_status_PMS_active, &_PMSAct);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_get_general_status", status, this);
        if(_PMSAct) {
            doCallbacksInt32((epicsInt32)_PMSAct, _interruptIdPmsAct);
            error();
            unlock();
            return ndsSuccess;
        }
        
        _UpdateReason = 0x0;
        if (_PulseTypeChanged && _OperatingMode == mode_normal) {
            if (commitPulseType((int) _PulseType) != ndsSuccess) {
                error();
                unlock();
                return ndsSuccess;
            }
            _UpdateReason |= SIS8300LLRFDRV_UPDATE_REASON_NEW_PT;
            _PulseTypeChanged = 0;

        }
        else {
            for (i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
                _UpdateReason |= _llrfChannelGroups[i]->getUpdateReason();
            }
        }
        
        NDS_DBG("CAlling update params with reason %#8x", _UpdateReason);
        status = sis8300llrfdrv_update(_DeviceUser, _UpdateReason);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_update", status, this);
        doCallbacksInt32((epicsInt32) _UpdateReason, _interruptIdUpdateReason);


        status = sis8300llrfdrv_get_general_status(_DeviceUser, gen_status_PMS_active, &_PMSAct);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_get_general_status", status, this);
        if(_PMSAct) {
            doCallbacksInt32((epicsInt32)_PMSAct, _interruptIdPmsAct);
            error();
            unlock();
            return ndsSuccess;
        }

        if (startAllGroups() != ndsSuccess) {
            error();
            unlock();
            return ndsSuccess;
        }

        unlock();
/*****
 * UNLOCKING THE DEVICE AND CG PORT!!!
 *****/
    }

    return ndsSuccess;
}

/* ==================== GETTERS, SETTERS, REGISTER HANDLERS ==================== */
/**
 * @brief Registers handlers for interfacing with records. For more information,
 * refer to NDS documentation.
 */
ndsStatus sis8300llrfDevice::registerHandlers(nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_FORCE_TRIGG,
            &sis8300llrfDevice::forceTrigger,
            &sis8300llrfDevice::getInt32,
            &_interruptIdForceTrigg);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_OPERATING_MODE,
            &sis8300llrfDevice::setOperatingMode,
            &sis8300llrfDevice::getOperatingMode,
            &_interruptIdOperatingMode);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_PULSE_TYPE,
            &sis8300llrfDevice::setPulseType,
            &sis8300llrfDevice::getPulseType,
            &_interruptIdPulseType);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_SETUP_ACTIVE,
            &sis8300llrfDevice::setSetupActive,
            &sis8300llrfDevice::getSetupActive,
            &_interruptIdSetupActive);

    /* only status records that process on I/O,
     * without getters and setters
     */
    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_PULSE_DONE_COUNT,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdPulseDoneCount);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_PULSE_MISSED,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdPulseMissed);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_UPDATE_REASON,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getUpdateReason,
            &_interruptIdUpdateReason);
            
    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_STATUS,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdStatus);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_ARM,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdArm);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_PULSE_DONE,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdPulseDone);
            
    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_PMS_ACT,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdPmsAct);
            
    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_SIGNAL_ACTIVE,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdSignalActive);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfDevice::PV_REASON_F_SAMPLING,
            &sis8300llrfDevice::setFloat64, &sis8300llrfDevice::getFSampling,
            &_interruptIdFSampling);

    return sis8300Device::registerHandlers(pvContainers);
}

/**
 * @brief Commit new pulse type to the device
 *
 * @param [in]  pulseType   Pulset type to set
 *
 * @return ndsSuccess   pulse type changed
 * @return ndserror     If one of hte SP or FF table groups reported that pulseType
 *                      is not set up or if the write to the device failed
 */
ndsStatus sis8300llrfDevice::commitPulseType(int pulseType) {
    int status;
    int activeTable;

    NDS_INF("Commiting Pulse Type");

    /* set the new pulse type - new active table */
    activeTable = _CgSP->getActiveTable();
    if (_CgSP->setActiveTable(pulseType) != ndsSuccess) {
        NDS_ERR("setNewPulseParams failed");
        SIS8300NDS_MSGERR("set new pulse params failed");
        return ndsError;
    }

    if (_CgFF->setActiveTable(pulseType) != ndsSuccess) {
        /* Set the SP back to previous, so they are the same */
        _CgSP->setActiveTable(activeTable);
        NDS_ERR("setNewPulseParams failed");
        SIS8300NDS_MSGERR("set new pulse params failed");
        return ndsError;
    }

    status = sis8300llrfdrv_set_pulse_type(_DeviceUser, (unsigned) pulseType);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_set_pulse_type", status);
    doCallbacksInt32((epicsInt32) pulseType, _interruptIdPulseType);

    return ndsSuccess;
}

/**
 * @brief Set pulse type for the device
 *
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value
 *
 * @retval ndsError     pulse type out of range
 * @retval ndsSuccess    Set successfull
 */
ndsStatus sis8300llrfDevice::setPulseType(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    switch (getCurrentState()) {
        case nds::DEVICE_STATE_INIT:
        case nds::DEVICE_STATE_ON:
            break;
        default:
            SIS8300NDS_MSGERR("Cannot change pulse type if not in INIT or ON");
            NDS_ERR("Cannot change pulse type if not in INIT or ON");
            return ndsError;
    }

    if (_OperatingMode != mode_normal) {
        if (pasynUser != NULL) {
            NDS_ERR("Pulse type can only be selected in NORMAL mode");
            SIS8300NDS_MSGERR("Pulse type can only be selected in NORMAL mode");
            return ndsError;
        }
    }
    else {
        if ( value > _MaxPulseType || value < 0) {
            NDS_ERR("Pulse type is out of range");
            SIS8300NDS_MSGERR("Pulse type is out of range");
            return ndsError;
        }
        
        _PulseType = value;
        _PulseTypeChanged = 1;
    }

    if (getCurrentState() == nds::DEVICE_STATE_INIT) {
        return commitPulseType((int) value);
    }

    return ndsSuccess;
}

/**
 * @brief get pulse type for the device
 *
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value
 *
 * @retval ndsSuccess If not at IOC init
 * @retval ndsError      At IOC init
 */
ndsStatus sis8300llrfDevice::getPulseType(asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        return ndsError;
    }

    *value = _PulseType;

    return ndsSuccess;
}

/**
 * @brief Force a specific trigger. This function can only be used in
 *        special operating modes and will fail if device is in normal
 *        operating mode.
 *
 * @param [in]  pasynUser   asynUser context
 * @param [in]  value       trigger to force, see #sis8300llrfdrv.h
 *
 * @return ndsSuccess   all is well
 * @return ndsError     Either the device is in normal operating mode or
 *                      write to the device failed
 */
ndsStatus sis8300llrfDevice::forceTrigger(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

	int statusLib, i;
    ndsStatus statusNds;

	if (_SetupActive) {
        if (getCurrentState() != nds::DEVICE_STATE_INIT) {
            NDS_ERR("Are you trying to force triggers in error state?");
            SIS8300NDS_MSGERR("Are you trying to force triggers in error state?");
            return ndsError;
        }

        if ( (sis8300llrfdrv_trigger) value == trig_pulse_comming) {
            /* This mode is special. It does not arm and init done by
             * default because we are not in on state. Additionaly we
             * need an unlocked arm because we want to be able to
             * change the RTM settings in the process */
            
            statusLib = sis8300llrfdrv_init_done(_DeviceUser);
            SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_init_done", statusLib);

            statusLib = sis8300llrfdrv_arm_device_unlocked(_DeviceUser);
            SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_arm_device_unlocked", statusLib);
        }
    }
	else if (_OperatingMode == mode_normal) {
	    NDS_ERR("Cannot force trigger in NORMAL mode");
	    SIS8300NDS_MSGERR("Cannot force trigger in NORMAL mode");
	    return ndsError;
	}
	else if (getCurrentState() != nds::DEVICE_STATE_ON) {
	    NDS_ERR("Cannot force trigger when in special mode and not in ON");
        SIS8300NDS_MSGERR("Cannot force trigger when in special mode and not in ON");
        return ndsError;
	}
    else if ( (sis8300llrfdrv_trigger) value == trig_update_params ) {
        for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {                
            
            statusNds = _llrfChannelGroups[i]->forceCommitParameters();
            if (statusNds != ndsSuccess) {
                NDS_ERR("UPDATE PARAMS write failed: %s", 
                _llrfChannelGroups[i]->getName().c_str());
                SIS8300NDS_MSGERR("Update params write failed.");
                return ndsError;
            }
            
            /* DISCUSSION PENDING: is it enough to limit this commit to controller channel group 
            if (_CgCtrl->forceCommitParameters() != ndsSuccess) {
                
            }
            */
        }    
    }

    statusLib = sis8300llrfdrv_force_trigger(_DeviceUser, (sis8300llrfdrv_trigger) value);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_force_trigger", statusLib);
    
    doCallbacksInt32(value, _interruptIdForceTrigg);
    
    return checkSignalStatus();
}

/**
 * @brief   Select device operating mode
 *
 * @param [in]  pasynUser anynUser context
 * @param [in]  value     mode to select, see #sis8300llrfdrv_special_operation.h
 *
 * @return ndsSuccess     mode change successfully
 * @return ndsError       Change failed, either because state is not init or write to the device failed
 *
 *
 * This will allow changing of the operating mode during INIT state. Calibration mode is special 
 * beacuse it allows writing new parameters to an armed device. Other modes strictly forbid this
 */
ndsStatus sis8300llrfDevice::setOperatingMode(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    int status;

    if (getCurrentState() != nds::DEVICE_STATE_INIT) {
        SIS8300NDS_MSGERR("Operating mode can only be changed in INIT");
        NDS_ERR("Operating mode can only be changed in INIT");
        return ndsError;
    }

    status = sis8300llrfdrv_set_operating_mode(_DeviceUser, (sis8300llrfdrv_operating_mode) value);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_set_operating_mode", status);

    _OperatingMode = (sis8300llrfdrv_operating_mode) value;
    doCallbacksInt32((epicsInt32) _OperatingMode, _interruptIdOperatingMode);

    return ndsSuccess;
}

/**
 * @brief get the Opearting mode
 *
 * @param [in]  pasynUser   asunUser context
 * @param [out] value       Will hold the mode on success
 *
 * @return ndsError     Device is in IOC_INIT stae
 * @return ndsSuccess   Otherwise
 */
ndsStatus sis8300llrfDevice::getOperatingMode(asynUser *pasynUser, epicsInt32 *value) {
    if (getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        return ndsError;
    }

    *value = (epicsInt32) _OperatingMode;

    return ndsSuccess;
}

/**
 * @brief Get the update reason that was sent to the device before the last
 *        arm. 
 * 
 * @param
 * 
 * The update reason is used to force the controller to load new values from
 * shadow registers or to reload control tables when the controller is running
 * = when this device is in ON state.
 */
ndsStatus sis8300llrfDevice::getUpdateReason(asynUser *pasynUser, epicsInt32 *value) {
	if (getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        return ndsError;
    }

    *value = (epicsInt32) _UpdateReason;

    return ndsSuccess;
}

ndsStatus sis8300llrfDevice::getFSampling(asynUser *pasynUser, epicsFloat64 *value) {
    *value = _FSampling;

    return ndsSuccess;
}


/**
 * @brief This flag is used to indicate that the device is in setup mode. 
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [in]  value       Set to 1 = ENABLE SETUP, 0 = DISABLE SETUP
 * 
 * @return ndsSuccess   Setup activated
 * @return ndsError     If setup is requested from any other state than INIT
 *                      or if the current operating mode is not normal.
 * 
 * This mode is used for initial setup of the device, where loop
 * parameters and input signals are being set up.
 */
ndsStatus sis8300llrfDevice::setSetupActive(asynUser *pasynUser, epicsInt32 value) {

    if (value) {

        if (getCurrentState() != nds::DEVICE_STATE_INIT) {
            SIS8300NDS_MSGERR("Setup can only be activated when in INIT");
            NDS_ERR("Setup can only be activated when in INIT");
            return ndsError;
        }

        if (_OperatingMode != mode_normal) {
            SIS8300NDS_MSGERR("Setup can only be activated in NORMAL operating mode");
            NDS_ERR("Setup can only be activated when in NORMAL operating mode");
            return ndsError;
        }
    }

    _SetupActive = value ? 1 : 0;

    doCallbacksInt32(_SetupActive, _interruptIdSetupActive);

    return ndsSuccess;
}
/**
 * @brief Check if the device is in setup mode
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Ons success this will be 1 = ENABLED, 0 = DISABLED
 * 
 * @return ndsError     If call is made during IOC INIT
 * @return ndsSuccess   All other cases
 */
ndsStatus sis8300llrfDevice::getSetupActive(asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        return ndsError;
    }

    *value = _SetupActive;

    return ndsSuccess;
}

/**
 * @brief Start processing of all channel groups
 *
 * @return statusSuccess    Groups started
 * @return statusError      One or more grpous returned error
 * 
 */
inline ndsStatus sis8300llrfDevice::startAllGroups() {
    NDS_TRC("%s", __func__);

    int i;
    ndsStatus statusCombined = ndsSuccess;
    nds::ChannelStates cgState;

    for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
        
        cgState = _llrfChannelGroups[i]->getCurrentState();
        
        if (cgState == nds::CHANNEL_STATE_ERROR) {
            NDS_ERR("Start CG failed: %s in ERR state!", 
                _llrfChannelGroups[i]->getName().c_str());
            SIS8300NDS_MSGERR("Start CG failed, CG in ERR!");
            statusCombined = ndsError;
        }
        else if (cgState != nds::CHANNEL_STATE_PROCESSING) {
            if (_llrfChannelGroups[i]->start() != ndsSuccess) {
                NDS_ERR("Start CG failed: %s", 
                    _llrfChannelGroups[i]->getName().c_str());
                SIS8300NDS_MSGERR("Start CG failed!");
                statusCombined = ndsError;
            }
        }
    }
    
    cgState = _CgAI->getCurrentState();
    
    if (cgState == nds::CHANNEL_STATE_ERROR) {
        NDS_ERR("Start CG failed: %s, group in ERR state.", 
            _CgAI->getName().c_str());
        SIS8300NDS_MSGERR("Start CG failed, CG is in ERROR.");
        statusCombined = ndsError;
    }
    else if (cgState != nds::CHANNEL_STATE_PROCESSING) {
        if (_CgAI->start() != ndsSuccess) {
            NDS_ERR("Start CG failed: %s", _CgAI->getName().c_str());
            SIS8300NDS_MSGERR("Start CG failed!");
            statusCombined = ndsError;
        }
    }

    return statusCombined;
}

/**
 * @brief Stop processing of all channel groups
 *
 * @return statusSuccess    Groups stoppped
 * @return statusError      One or more groups returned error
 */
inline ndsStatus sis8300llrfDevice::stopAllGroups() {
    NDS_TRC("%s", __func__);

    int i;
    ndsStatus statusCombined = ndsSuccess;
    nds::ChannelStates cgState;

    for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
        
        cgState = _llrfChannelGroups[i]->getCurrentState();
        
        if (cgState == nds::CHANNEL_STATE_ERROR) {
            NDS_ERR("Stop CG failed: %s in ERR state!", 
                _llrfChannelGroups[i]->getName().c_str());
            SIS8300NDS_MSGERR("Stop CG failed, CG in ERR!");
            statusCombined = ndsError;
        }
        else if (cgState != nds::CHANNEL_STATE_DISABLED) {
            if (_llrfChannelGroups[i]->stop() != ndsSuccess) {
                NDS_ERR("Stop failed: %s", 
                    _llrfChannelGroups[i]->getName().c_str());
                SIS8300NDS_MSGERR("Stop CG failed!");
                statusCombined = ndsError;
            }
        }
    }
    
    cgState = _CgAI->getCurrentState();
    
    if (cgState == nds::CHANNEL_STATE_ERROR) {
        NDS_ERR("Stop failed: %s, group in ERR state.", 
            _CgAI->getName().c_str());
        SIS8300NDS_MSGERR("Stop CG failed, CG is in ERROR.");
        statusCombined = ndsError;
    }
    else if (cgState != nds::CHANNEL_STATE_DISABLED) {
        if (_CgAI->stop() != ndsSuccess) {
            NDS_ERR("STOP FAILED: %s", _CgAI->getName().c_str());
            SIS8300NDS_MSGERR("Stop CG failed");
            statusCombined = ndsError;
        }
    }

    return statusCombined;
}

/**
 * @brief Request a specific state for al lthe llrf groups
 *
 * @param [in] state to request
 *
 * @return ndsSuccess   Always
 */
ndsStatus sis8300llrfDevice::llrfCgStateRequest(nds::ChannelStates state){
    NDS_TRC("%s", __func__);

    int i;

    for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
        _llrfChannelGroups[i]->getCurrentStateObj()->requestState(
                _llrfChannelGroups[i], state);
    }

    return ndsSuccess;
}

/**
 * @brief Set a specific state for all lthe llrf groups
 *
 * @param [in] state to request
 *
 * @return ndsSuccess   Always
 */
ndsStatus sis8300llrfDevice::llrfCgStateSet(nds::ChannelStates state){
    NDS_TRC("%s", __func__);

    int i;

    for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
        _llrfChannelGroups[i]->getCurrentStateObj()->setMachineState(
                _llrfChannelGroups[i], state);
    }

    return ndsSuccess;
}


/**
 * @brief Force logging of a set of predefined raw registers if they are loaded
 */
inline void sis8300llrfDevice::logGenericPredefinedRegisters() {
    /* This uses the sis8300RegisterChannel to get the raw register value.
     * The channel number corresponds to register address
     */
    ndsStatus status;

    nds::Channel *chan;

    epicsUInt32 value;
    epicsUInt32 mask = 0xffffffff;
    int reg_addr;

    status = getChannel(_CgReg->getName(), (int) 0x10, &chan);
    if (status != ndsSuccess) {
        //channel does not exist, the template is most likely not loaded.
        //abort
        return;
    }

    (dynamic_cast<sis8300RegisterChannel *>(chan))->getValueUInt32Digital(NULL, &value, mask);

    //get the values of adc channel addresses
    for (reg_addr = 0x120; reg_addr <= 0x129; reg_addr++) {
        status = getChannel(_CgReg->getName(), reg_addr, &chan);
        if (status != ndsSuccess) {
            return;
        }
        (dynamic_cast<sis8300RegisterChannel *>(chan))->getValueUInt32Digital(NULL, &value, mask);
    }

    status = getChannel(_CgReg->getName(), (int) 0x220, &chan);
    if (status != ndsSuccess) {
        return;
    }
    (dynamic_cast<sis8300RegisterChannel *>(chan))->getValueUInt32Digital(NULL, &value, mask);

    return;
}

/** Brief Get firmware version of the SIS8300 device has available software support
*
* @param[in]	fwMajor				Firmware major version
* @param[in]	fwMinor				Firmware minor version
* @return	ndsSuccess              Firmware version is suported by current IOC version. 
* @return	ndsError                Firmware version is unsupported by current IOC version.
*/
ndsStatus sis8300llrfDevice::check_fw_version(unsigned fwMajor, unsigned fwMinor) {
  NDS_TRC("%s", __func__);
  NDS_DBG("Checking fw vesion v%u.%u is compatible with current IOC release.", fwMajor, fwMinor);
  /* Supported firmware versions
   * Rows correspond to major versions (0,1,2 etc.)
   * Columns have the following format
   * Column 0 is the first minor number supported by the current IOC version for the corresponding major version.
   * Column 1 is the last minor number supported by the current IOC version for the corresponding major version.
   * Column 2 is zero if no minor versions corresponding to this major number are supported by the current IOC version and non-zero where the specified minor version range is valid."
  */
  unsigned fwSupported[][3] = {
  {0,0,0},	// Major version 0.
  {0,0,0},
  {0,0,0},
  {0,0,1},
  {0,0,0} // Major version n.
  };
  /* Maximum supported major version (derived from fwSupported array) */
  unsigned sMajorMax;
  bool bMaxAny;

  /* Minor version range supported */
  unsigned sMinorFirst, sMinorLast;
  if (sizeof(*fwSupported) > 0) {
    sMajorMax = sizeof(fwSupported)/sizeof(*fwSupported) - 1;
    bMaxAny=fwSupported[fwMajor][2] > 0;
    NDS_DBG("Firmware major max supported is %u and valid minor versions present = %u", sMajorMax, bMaxAny); 
  } else {
    NDS_ERR("Could not retrieve supported firmware information within IOC. Contact IOC developer for support.");
    return ndsError;
  }
  /* Check major version is supported */
  if (fwMajor > sMajorMax || !bMaxAny ) {
    NDS_DBG("Firmware major version number %u unsupported by current IOC release", fwMajor);
    NDS_ERR("Firmware version v%u.%u is unsupported by current IOC version. Contact IOC developer for support.", fwMajor, fwMinor);
    return ndsError;
  }
  /* Check minor version support */
  sMinorLast = *(*(fwSupported + fwMajor) + 1);
  sMinorFirst = *(*(fwSupported + fwMajor));
  NDS_DBG("For firmware major version %u, minor versions %u to %u are supported.", fwMajor, sMinorFirst, sMinorLast);
  if (fwMinor >= sMinorFirst && fwMinor <= sMinorLast) {
    NDS_INF("Firmware version v%u.%u is compatible with current IOC version.", fwMajor, fwMinor);
    return ndsSuccess;
  } else {
    NDS_DBG("Firmware minor version number %u unsupported by current IOC release", fwMinor);
    NDS_ERR("Firmware version v%u.%u is unsupported by current IOC version. Contact IOC developer for support.", fwMajor, fwMinor);
    return ndsError;
  }
}
