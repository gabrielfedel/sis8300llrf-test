/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfChannel.cpp
 * @brief Implementation of LLRF specific channel in NDS.
 * @author urojec
 * @date 5.6.2014
 * 
 * This channel is only used for cavity (AI0) and reference (AI1) 
 * signals and adds the functionality of reading out the current
 * Magnitude and Angle (MA) point. IQ point is also callculated
 * from the MA point by software. 
 * 
 * The class also makes sure that the two channels never get disabled.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfAIChannel.h"

#include "math.h"


std::string sis8300llrfAIChannel::PV_REASON_SIGNAL_ANGLE = "SignalAngle";
std::string sis8300llrfAIChannel::PV_REASON_SIGNAL_MAG   = "SignalMagnitude";
std::string sis8300llrfAIChannel::PV_REASON_SIGNAL_I     = "SignalI";
std::string sis8300llrfAIChannel::PV_REASON_SIGNAL_Q     = "SignalQ";
std::string sis8300llrfAIChannel::PV_REASON_NEW_MA_POINT = "NewMAPoint";

/**
 * @brief llrf channel constructor.
 */
sis8300llrfAIChannel::sis8300llrfAIChannel() : sis8300AIChannel() {
    _isEnabled = 1;
}


sis8300llrfAIChannel::sis8300llrfAIChannel(epicsFloat64 FSampling) : sis8300AIChannel(FSampling) {
    _isEnabled = 1;
}

/**
 * @brief LLRF AI channel destructor.
 *
 * Free channel data buffer.
 */
sis8300llrfAIChannel::~sis8300llrfAIChannel() {}

/**
 * @brief In addition to getting the wf data as parent, 
 * it also reads angle and mag of the signal
 *
 * @see #sis8300AIChannel::onLeaveProcessing
 */
ndsStatus sis8300llrfAIChannel::onLeaveProcessing(
    nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s",__func__);
    if (readNewMAPoint(NULL, 1) != ndsSuccess) {
		return ndsError;
	}
    
    return sis8300AIChannel::onLeaveProcessing(from, to);
}

/**
 * @brief Override parent, because we are not allowed to disable 
 *        reference and cavity channel. The PI loop does not 
 *        even start with AI0 disabled.
 */
ndsStatus sis8300llrfAIChannel::setEnabled(
    asynUser *pasynUser, epicsInt32 value) {

    NDS_TRC("%s",__func__);
    if (value == 0) {
        NDS_ERR("AI Channel 0 (CAV) and REF (1) are always enabled");
        return ndsError;
    }

    return sis8300AIChannel::setEnabled(pasynUser, 1);
}

/**
 * @brief Registers handlers for interfacing with records. 
 * For more information, refer to NDS documentation.
 */
ndsStatus sis8300llrfAIChannel::registerHandlers(
    nds::PVContainers* pvContainers) {
    
    NDS_TRC("%s",__func__);
    NDS_PV_REGISTER_FLOAT64(
         sis8300llrfAIChannel::PV_REASON_SIGNAL_ANGLE,
        &sis8300llrfAIChannel::setFloat64, 
        &sis8300llrfAIChannel::getFloat64,
        &_interruptIdSignalAngle);

    NDS_PV_REGISTER_FLOAT64(
         sis8300llrfAIChannel::PV_REASON_SIGNAL_MAG,
        &sis8300llrfAIChannel::setFloat64, 
        &sis8300llrfAIChannel::getFloat64,
        &_interruptIdSignalMag);
        
    NDS_PV_REGISTER_FLOAT64(
         sis8300llrfAIChannel::PV_REASON_SIGNAL_I,
        &sis8300llrfAIChannel::setFloat64, 
        &sis8300llrfAIChannel::getFloat64,
        &_interruptIdSignalI);

    NDS_PV_REGISTER_FLOAT64(
         sis8300llrfAIChannel::PV_REASON_SIGNAL_Q,
        &sis8300llrfAIChannel::setFloat64, 
        &sis8300llrfAIChannel::getFloat64,
        &_interruptIdSignalQ);
        
    NDS_PV_REGISTER_INT32(
         sis8300llrfAIChannel::PV_REASON_NEW_MA_POINT,
        &sis8300llrfAIChannel::readNewMAPoint, 
        &sis8300llrfAIChannel::getInt32,
        &_interruptIdNewMAPoint);

    return sis8300AIChannel::registerHandlers(pvContainers);
}

/**
 * @brief Read new MA (Magnitude and Anlge) point for the AI signal. 
 * When a new MA point is recieved, IQ values are callculated also. 
 * 
 * @param [in]  pasynUser   Pointer to asyn user context
 * @param [in]  value       Does not matter. Call to this function will 
 *                          always process
 * 
 * @return ndsError         Read from the device failed
 * @return ndsSuccess       Data retrieved successfully
 * 
 * 
 * When reading a MA point, the IQ point is also callculated. All the 
 * data is correlated, so user should update the point when new MA point 
 * interrupt arrives. All the records listening to 
 * #PV_REASON_SIGNAL_ANGLE, #PV_REASON_SIGNAL_MAG, #PV_REASON_SIGNAL_I 
 * and #PV_REASON_SIGNAL_Q will get processed. There are no separate 
 * getters for these values, because they should always be read out 
 * together.
 */
ndsStatus sis8300llrfAIChannel::readNewMAPoint(
    asynUser* pasynUser, epicsInt32 value) {
    
    NDS_TRC("%s",__func__);
    int status;
    double angle, magnitude, i, q;
    
    status = sis8300llrfdrv_get_signal_ma(_DeviceUser, 
                getChannelNumber(), &angle, &magnitude);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_signal_ma", status);

    i = magnitude * cos(angle);
    q = magnitude * sin(angle);

    doCallbacksFloat64((epicsFloat64) magnitude, _interruptIdSignalMag);
    doCallbacksFloat64((epicsFloat64) angle, _interruptIdSignalAngle);
    doCallbacksFloat64((epicsFloat64) i, _interruptIdSignalI);
    doCallbacksFloat64((epicsFloat64) q, _interruptIdSignalQ);
    
    doCallbacksInt32((epicsInt32) 1, _interruptIdNewMAPoint);

    return ndsSuccess;
}
