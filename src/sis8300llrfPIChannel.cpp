/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIChannel.cpp
 * @brief Implementation of PI input channel in NDS.
 * @author urojec
 * @date 26.5.2014
 * 
 * This class exposes all the settings available for the PI controller 
 * part on the device. There are always two instances of the class - one 
 * for I and one for Q PI controller. The settings are the same for both 
 * controllers. The class also reads out data that changes on 
 * pulse-to-pulse basis, which includes: PI error waveform and PI 
 * overflow status. This is done in the #onLeaveProcessing function.
 * 
 * RMS statistics:
 * Apart from device related settings and readouts, this class also 
 * tracks the RMS values of the PI error waveform that is recieved for 
 * each pulse and does a cumulative average. The average is reset when 
 * ANY (not just related to this class) parameters change, which is 
 * determined from the update reason in #onEneterProcessing. The
 * RMS averages can also be reset on demand.
 */

#include <math.h>

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"
#include "sis8300llrfPIChannel.h"

#define ROUNDUP_TWOHEX(val) ((unsigned) (val + 0x1F) &~0x1F)

std::string sis8300llrfPIChannel::
                    PV_REASON_PI_GAIN_K          = "PIGainK";
std::string sis8300llrfPIChannel::
                    PV_REASON_PI_GAIN_TS_DIV_TI  = "PIGainTsDivTi";
std::string sis8300llrfPIChannel::
                    PV_REASON_SAT_MAX            = "PISaturationMax";
std::string sis8300llrfPIChannel::
                    PV_REASON_SAT_MIN            = "PISaturationMin";
std::string sis8300llrfPIChannel::
                    PV_REASON_FIXED_SP_VAL       = "PIFixedSPVal";
std::string sis8300llrfPIChannel::
                    PV_REASON_FIXED_FF_VAL       = "PIFixedFFVal";
std::string sis8300llrfPIChannel::
                    PV_REASON_FIXED_SP_ENABLE    = "PIFixedSPEnable";
std::string sis8300llrfPIChannel::
                    PV_REASON_FIXED_FF_ENABLE    = "PIFixedFFEnable";

std::string sis8300llrfPIChannel::
                    PV_REASON_PI_OVERFLOW_STATUS = "PIOverflowStatus";
std::string sis8300llrfPIChannel::
                    PV_REASON_RMS_CURRENT        = "RMSCurrent";
std::string sis8300llrfPIChannel::
                    PV_REASON_RMS_SMNM_IGNORE    = "RMSSMNMIgnore";
std::string sis8300llrfPIChannel::
                    PV_REASON_RMS_AVERAGE        = "RMSAverage";
std::string sis8300llrfPIChannel::
                    PV_REASON_RMS_MAX            = "RMSMax";
std::string sis8300llrfPIChannel::
                    PV_REASON_RMS_PULSECNT       = "RMSPulseCnt";
std::string sis8300llrfPIChannel::
                    PV_REASON_RMS_RESET          = "RMSReset";

//TODO: pi type to string

/**
 * @brief PI channel constructor.
 */
sis8300llrfPIChannel::sis8300llrfPIChannel(sis8300llrfdrv_pi_type piType) :
        sis8300llrfChannel(
            SIS8300LLRFDRV_PI_PARAM_NUM, 
            SIS8300LLRFDRV_PI_PARAM_INT_FIRST) {

    _PIType = piType;

    switch (_PIType) {
    case pi_I:
        _GenStatusBit = gen_status_pi_overflow_I;
        strcpy(_ChanStringIdentifier, "PI I");
        _ConvOffset = SIS8300LLRFDRV_RAW_SAMPLE_I_OFFSET;
        break;
    case pi_Q:
        _GenStatusBit = gen_status_pi_overflow_Q;
        strcpy(_ChanStringIdentifier, "PI Q");
        _ConvOffset = SIS8300LLRFDRV_RAW_SAMPLE_Q_OFFSET;
        break;
    default:
        NDS_CRT("Invalid option for PI channel! Must be I or Q!");
        break;
    }

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfPIChannel::onLeaveProcessing, 
        this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfPIChannel::onEnterProcessing, 
        this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfPIChannel::onEnterReset, this));

    _ChannelData = NULL;

    _RMSNsamplesIgnore = 0;
    _RMSAverage        = 0.0;
    _RMSMax            = 0.0;
    _RMSPulseCnt       = 0;
    _RMSReset          = 0;
    _RMSCurrent        = 0.0;
}

/**
 * @brief AI channel destructor.
 *
 * Free channel data buffer.
 */
sis8300llrfPIChannel::~sis8300llrfPIChannel() {}

epicsFloat32 sis8300llrfPIChannel::_ConvFact = 
    1.0 / (epicsFloat32) (1 << sis8300llrfdrv_Qmn_IQ_sample.frac_bits_n);

/**
 * @see   sis8300llrfChannel:onEnterReset
 * @brief Aditianaly to parent, also reset the RMS values and check the
 *        PI overflow status
 */
ndsStatus sis8300llrfPIChannel::onEnterReset() {
    
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    if (checkStatuses() != ndsSuccess) {
        return ndsError;
    }
    
    if (setRMSReset(NULL, 1) != ndsSuccess) {
        return ndsError;
    }
    
    return ndsSuccess;
}
/**
 * @see   sis8300llrfChannel:onEnterProcessing
 * @brief Aditianaly to parent, also reset the RMS values if the 
 *        parameters have changed (=if the update reason was non-zero)
 */
ndsStatus sis8300llrfPIChannel::onEnterProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    
    epicsInt32 reason;
    
    /* we want to reset the statistics anytime a paramter changes.
     * the update reason will be non zero in that case */
     
    (dynamic_cast<sis8300llrfDevice *>(_device))
                                    ->getUpdateReason(NULL, &reason);
    
    return reason ? setRMSReset(NULL, 1) : ndsSuccess;
}
/**
 * @brief Read all the data from the controller and callculate the RMS
 *
 * @return ndsSuccess   Data read successfully
 * @return ndsError     Read failed, chan goes to ERR state or
 *                      couldn't allocate memory.
 *
 * This will read all the values and waveforms that get changed on 
 * pulse-2-pulse basis (current PI error and PI overflow status). It 
 * will also callculate the RMS from the recieved waveform.
 */
ndsStatus sis8300llrfPIChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
    int status, iterSrc, iterDest;

    epicsFloat32 sumSqrd;

    unsigned nsamples, nsamplesRampUp, nsamplesActive;

    epicsInt16 * rawDataI16;
    epicsFloat32 * convData;

    
    if (checkStatuses() != ndsSuccess) {
        return ndsError;
    }


    status = sis8300llrfdrv_get_acquired_nsamples(
                _DeviceUser, samples_cnt_pi_total, &nsamples);
    _SamplesCount = (epicsInt32) nsamples;
    doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);
    
    if (_SamplesCount == 0) {
        //TODO: FLAG THAT THERE WERE ZERO SAMPLES FOR RMS STATISTICS
        return ndsSuccess;
    }
    if (_SamplesCount > SIS8300LLRF_MAXPISAMPLES) {
      NDS_INF("Pulse took too long. PI Error Sample Count = %d", _SamplesCount);
      return ndsSuccess;
    }

    /* we need to read in 512 bit blocks - firmware imposed limitation */
    NDS_DBG("Rounding _SamplesCount from %d", _SamplesCount);
    nsamples = ROUNDUP_TWOHEX(_SamplesCount);
    NDS_DBG("_SamplesCount rounded to %u", nsamples);

    status = sis8300llrfdrv_get_acquired_nsamples(
                _DeviceUser, samples_cnt_pi_ramp_up_phase, &nsamplesRampUp);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_samples_cnt", status);

    status = sis8300llrfdrv_get_acquired_nsamples(
                _DeviceUser, samples_cnt_pi_active_phase, &nsamplesActive);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_samples_cnt", status);

    rawDataI16 = new (std::nothrow) epicsInt16[nsamples * 2];
    convData = new (std::nothrow) epicsFloat32[_SamplesCount];

    if (!rawDataI16 || !convData) 
        return ndsError;
    
    status = sis8300llrfdrv_read_pi_error_raw(
                _DeviceUser, rawDataI16, nsamples);
    //SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_read_pi_error_raw", status);

    /* read 32 raw samples as 16 bit - I and Q table are interleaved */
    sumSqrd = 0.0;
    for (iterSrc = _ConvOffset, iterDest = 0; 
            iterDest < _SamplesCount; iterDest++, iterSrc += 2 ) {
        convData[iterDest] = 
            _ConvFact * (epicsFloat32) rawDataI16[iterSrc];
        
        if (   iterDest >= (int)nsamplesRampUp 
            && iterDest < (_SamplesCount - _RMSNsamplesIgnore)) {
            sumSqrd += convData[iterDest] * convData[iterDest];
        }
    }
    
    /* THIS IS ALWAYS WRONG, nsamplesRampUp + nsamplesActive = 
     * nsamplesTotal + 1 
     * TODO: talk Fredrik
     * TODO: remove this, but right now it's extreamly annoying for 
     * development
     *
    if (nsamplesRampUp + nsamplesActive != (unsigned)_SamplesCount) {
        NDS_ERR("nsamplesRampUp(%u) + nsamplesActive(%u) != _SamplesCount(%u)",
                nsamplesRampUp, nsamplesActive, (unsigned)_SamplesCount);
    }
    */

    sumSqrd /= (epicsFloat32) 
        (_SamplesCount - _RMSNsamplesIgnore - nsamplesRampUp);
    
    _RMSCurrent = (epicsFloat64) sqrt((double) sumSqrd);
    _RMSPulseCnt++;
    _RMSAverage += (_RMSCurrent - _RMSAverage) / _RMSPulseCnt;
    _RMSMax = (_RMSCurrent > _RMSMax) ? _RMSCurrent : _RMSMax;
    
    doCallbacksFloat64(
        _RMSCurrent, _interruptIdRMSCurrent);
    doCallbacksInt32(
        _RMSPulseCnt, _interruptIdRMSPulseCount);
    doCallbacksFloat64(
        _RMSAverage, _interruptIdRMSAverage);
    doCallbacksFloat64(
        _RMSMax, _interruptIdRMSMax);
    doCallbacksInt32(
        (epicsInt32) _RMSNsamplesIgnore, _interruptIdValueInt32);    
    doCallbacksFloat32Array(
        convData, (size_t) _SamplesCount, _interruptIdBufferFloat32);

    delete [] rawDataI16;
    delete [] convData;

    return ndsSuccess;
}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfPIChannel::readParameter(
                int paramIdx, double *paramVal) {
    return sis8300llrfdrv_get_pi_param(
                _DeviceUser, _PIType, 
                (sis8300llrfdrv_pi_param) paramIdx, paramVal);
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfPIChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_pi_param(
                _DeviceUser, _PIType,
                (sis8300llrfdrv_pi_param) paramIdx, 
                _ParamVals[paramIdx], paramErr);
}

/**
 * @brief All the statuses that need to be checked on leave processing
 *        and enter reset
 */
inline ndsStatus sis8300llrfPIChannel::checkStatuses() {
    epicsInt32 valInt32;
    
    if (getPIOverflowStatus(NULL, &valInt32) != ndsSuccess) {
        return ndsError;
    }

    return ndsSuccess;
}

/**
 * @brief Get PI controller type
 *
 * @return pi_angle or pi_mag, @see #sis8300llrfdrv_pi_type
 */
sis8300llrfdrv_pi_type sis8300llrfPIChannel::getPIType() {
    return _PIType;
}

/**
 * @brief Read current PI overflow status from the device
 *
 * @param [in]  pasynUser Asyn user context struct
 * @param [out] value     On success this will be 1 if overflow occured 
 *                        and 0 if not
 *
 * @retval ndsSuccess    Value read successfully.
 * @retval ndsError      Could not retrieve the data from the device
 */
ndsStatus sis8300llrfPIChannel::getPIOverflowStatus(
                asynUser *pasynUser, epicsInt32 *value) {
    unsigned overflowStatus;
    int status;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300llrfdrv_get_general_status(
                _DeviceUser, _GenStatusBit, &overflowStatus);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_get_general_status", status);

    *value = (epicsInt32) overflowStatus;

    doCallbacksUInt32Digital(*value, _interruptIdPIOverflowStatus);

    return ndsSuccess;
}


/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfPIChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    
    /* RMS related */

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIChannel::PV_REASON_RMS_SMNM_IGNORE,
           &sis8300llrfPIChannel::setRMSSMNMIgnore,
           &sis8300llrfPIChannel::getRMSSMNMIgnore,
           &_interruptIdRMSSMNMIgnore
     );

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIChannel::PV_REASON_RMS_CURRENT,
           &sis8300llrfPIChannel::setFloat64,
           &sis8300llrfPIChannel::getFloat64,
           &_interruptIdRMSCurrent);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIChannel::PV_REASON_RMS_AVERAGE,
           &sis8300llrfPIChannel::setFloat64,
           &sis8300llrfPIChannel::getFloat64,
           &_interruptIdRMSAverage);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIChannel::PV_REASON_RMS_MAX,
           &sis8300llrfPIChannel::setFloat64,
           &sis8300llrfPIChannel::getFloat64,
           &_interruptIdRMSMax);

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIChannel::PV_REASON_RMS_PULSECNT,
           &sis8300llrfPIChannel::setInt32,
           &sis8300llrfPIChannel::getInt32,
           &_interruptIdRMSPulseCount);

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIChannel::PV_REASON_RMS_RESET,
           &sis8300llrfPIChannel::setRMSReset,
           &sis8300llrfPIChannel::getInt32,
           &_interruptIdRMSReset);

    /* Device Settings */
    
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_PI_GAIN_K,
            &sis8300llrfPIChannel::setPIGainK,
            &sis8300llrfPIChannel::getPIGainK,
            &_interruptIds[pi_param_gain_K]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_PI_GAIN_TS_DIV_TI,
            &sis8300llrfPIChannel::setPIGainTsDivTi,
            &sis8300llrfPIChannel::getPIGainTsDivTi,
            &_interruptIds[pi_param_gain_TSdivTI]);

    NDS_PV_REGISTER_FLOAT64(sis8300llrfPIChannel::PV_REASON_SAT_MAX,
            &sis8300llrfPIChannel::setPISaturationMax,
            &sis8300llrfPIChannel::getPISaturationMax,
            &_interruptIds[pi_param_sat_max]);

    NDS_PV_REGISTER_FLOAT64(sis8300llrfPIChannel::PV_REASON_SAT_MIN,
            &sis8300llrfPIChannel::setPISaturationMin,
            &sis8300llrfPIChannel::getPISaturationMin,
            &_interruptIds[pi_param_sat_min]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_FIXED_SP_VAL,
            &sis8300llrfPIChannel::setPIFixedSPVal,
            &sis8300llrfPIChannel::getPIFixedSPVal,
            &_interruptIds[pi_param_fixed_sp_val]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_FIXED_FF_VAL,
            &sis8300llrfPIChannel::setPIFixedFFVal,
            &sis8300llrfPIChannel::getPIFixedFFVal,
            &_interruptIds[pi_param_fixed_ff_val]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfPIChannel::PV_REASON_FIXED_SP_ENABLE,
            &sis8300llrfPIChannel::setPIFixedSPEnable,
            &sis8300llrfPIChannel::getPIFixedSPEnable,
            &_interruptIds[pi_param_fixed_sp_en]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfPIChannel::PV_REASON_FIXED_FF_ENABLE,
            &sis8300llrfPIChannel::setPIFixedFFEnable,
            &sis8300llrfPIChannel::getPIFixedFFEnable,
            &_interruptIds[pi_param_fixed_ff_en]);

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIChannel::PV_REASON_PI_OVERFLOW_STATUS,
	   &sis8300llrfPIChannel::setInt32,
           &sis8300llrfPIChannel::getPIOverflowStatus,
           &_interruptIdPIOverflowStatus
     );

    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ============ RELATED TO PI ERROR RMS=============== */
/**
 * @brief Write 1 to reset the RMS value
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set to 1 to reset the RMS
 * 
 * @return ndsSuccess   Always
 * 
 * If the value is reset, than the RMS cumulative average will
 * be reset with the next pulse and started again
 */
ndsStatus sis8300llrfPIChannel::setRMSReset(
                asynUser *pasynUser, epicsInt32 value) {

    if (value) {
        _RMSPulseCnt = 0;
        _RMSAverage = 0.0;
        _RMSMax = 0.0;
        doCallbacksInt32(_RMSPulseCnt, _interruptIdRMSPulseCount);
        doCallbacksFloat64(_RMSAverage, _interruptIdRMSAverage);
        doCallbacksFloat64(_RMSMax, _interruptIdRMSMax);
    }

    return ndsSuccess;
}

/**
 * @brief Specify the number of PI erros to ignore at the end of the 
 *        acquired PI error waveform when callculating the RMS.
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Number of samples to ignore
 * 
 * @return  ndsError    If number of samples in less than zero
 * @return  ndsSuccess  All other cases
 */
ndsStatus sis8300llrfPIChannel::setRMSSMNMIgnore(
                asynUser *pasynUser, epicsInt32 value) {
    if (_RMSNsamplesIgnore < 0) {
        return ndsError;
    }

    _RMSNsamplesIgnore = (int) value;

    return ndsSuccess;
}
/**
 * @brief Get the number of samples that are ignored at the end of the 
 *        acquired PI error waveform when RMS is being callculated.
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Will hold the number of ignored samples on 
 *                          success
 * 
 * @return  ndsError    If this call is made during IOC INIT
 * @return  ndsSuccess  All other cases
 */
ndsStatus sis8300llrfPIChannel::getRMSSMNMIgnore(
                asynUser *pasynUser, epicsInt32 *value) {
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _RMSNsamplesIgnore;

    return ndsSuccess;
}


/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ============ RELATED TO PI CONTROLLER ============= */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set PI controller gain K
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the K gain to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIGainK(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_gain_K] = (double) value;

    _ParamChanges[pi_param_gain_K] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller K gain
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the K gain value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIGainK(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_gain_K];

    return ndsSuccess;
}
/**
 * @brief Set PI controller gain Ts/Ti
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the Ts/Ti gain to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIGainTsDivTi(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_gain_TSdivTI] = (double) value;

    _ParamChanges[pi_param_gain_TSdivTI] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller Ts/Ti gain
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the Ts/Ti gain value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIGainTsDivTi(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_gain_TSdivTI];
    return ndsSuccess;
}
/**
 * @brief Set PI controller maximum saturation value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the maximum saturation to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPISaturationMax(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_sat_max] = (double) value;

    _ParamChanges[pi_param_sat_max] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller maximum saturation setting
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the max saturation value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPISaturationMax(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_sat_max];

    return ndsSuccess;
}
/**
 * @brief Set PI controller minimum saturation value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the minimum saturation to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPISaturationMin(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_sat_min] = (double) value;

    _ParamChanges[pi_param_sat_min] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller minimuam saturation setting
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the min saturation value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPISaturationMin(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_sat_min];

    return ndsSuccess;
}
/**
 * @brief Set PI controller fixed SP value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the fixed SP to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIFixedSPVal(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_fixed_sp_val] = (double) value;

    _ParamChanges[pi_param_fixed_sp_val] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller fixed SP value setting
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current fixed SP value on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIFixedSPVal(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_fixed_sp_val];
    return ndsSuccess;
}
/**
 * @brief Set PI controller fixed FF value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the fixed FF to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIFixedFFVal(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_fixed_ff_val] = (double) value;

    _ParamChanges[pi_param_fixed_ff_val] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller fixed FF value setting
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current fixed FF value on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIFixedFFVal(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_fixed_ff_val];
    return ndsSuccess;
}
/**
 * @brief Enable fixed SP
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIFixedSPEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_fixed_sp_en] = value ? 1 : 0;

    _ParamChanges[pi_param_fixed_sp_en] = 1;
    return commitParameters();
}

/**
 * @brief Check if fixed SP is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIFixedSPEnable(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[pi_param_fixed_sp_en];

    return ndsSuccess;
}

/**
 * @brief Enable fixed FF
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIFixedFFEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_fixed_ff_en] = value ? 1 : 0;

    _ParamChanges[pi_param_fixed_ff_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if fixed FF is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIFixedFFEnable(
                asynUser *pasynUser, epicsInt32 *value) {
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[pi_param_fixed_ff_en];

    return ndsSuccess;
}
