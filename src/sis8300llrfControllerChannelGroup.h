/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIChannelGroup.h
 * @brief Header file defining the LLRF PI channelGroup class
 * @author urojec
 * @date 23.5.2014
 */

#ifndef _sis8300llrfControllerChannelGroup_h
#define _sis8300llrfControllerChannelGroup_h

#include "sis8300llrfChannelGroup.h"

/**
 * @brief sis8300 specific nds::ChannelGroup class that supports LLRF PI
 */
class sis8300llrfControllerChannelGroup: public sis8300llrfChannelGroup {

public:
    sis8300llrfControllerChannelGroup(
            const std::string& name, sis8300drv_usr *newDeviceUser);
    virtual ~sis8300llrfControllerChannelGroup();
    
    virtual ndsStatus readParameters();
    virtual ndsStatus markAllParametersChanged();

    virtual ndsStatus setTriggerType(
                asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setOutputType(
                asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getOutputType(
                asynUser *pasynUser, epicsInt32 *value);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    void setN(epicsInt32 N);
    epicsInt32 getN();


protected:
    static std::string PV_REASON_SAMPLES_CNT_PI_RAMP_UP;
    static std::string PV_REASON_SAMPLES_CNT_PI_ACTIVE;
    static std::string PV_REASON_SAMPLES_CNT_PI_TOTAL;
    static std::string PV_REASON_SAMPLES_CNT_ADC_TOTAL;
    static std::string PV_REASON_SIGNAL_ANGLE_CAVITY;
    static std::string PV_REASON_SIGNAL_ANGLE_REFERENCE;
    static std::string PV_REASON_OUTPUT_TYPE;

    int _interruptIds[SIS8300LLRFDRV_SAMPLES_CNT_NUM];
    int _interruptIdSignalAngleCavity;
    int _interruptIdSignalAngleReference;
    int _interruptIdSignalMagCavity;
    int _interruptIdSignalMagReference;
    int _interruptIdOutputType;


    int _TriggerTypeChanged;
    int _OutputTypeChanged;

    static double _Rad2Deg;

    epicsInt32  _OutputType;
    epicsInt32  _N;

    virtual ndsStatus onLeaveProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
    
    /* write to hardware */
    virtual ndsStatus writeToHardware();
};

#endif /* _sis8300llrfControllerChannelGroup_h */
