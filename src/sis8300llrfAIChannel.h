/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file _sis8300llrfAIChannel_h
 * @brief Header file defining the LLRF AI channel class
 * @author urojec
 * @date 12.5.2015
 */

#ifndef _sis8300llrfAIChannel_h
#define _sis8300llrfAIChannel_h

#include "sis8300AIChannel.h"


/**
 * @brief llrf specific AI channel, used for AI0 and AI1 - reference and 
 * cavity. It prevents disabling the channel and provides signal 
 * magnitude and angle reads.
 */
class sis8300llrfAIChannel: public sis8300AIChannel {

public:
    sis8300llrfAIChannel(epicsFloat64 FSampling);
    sis8300llrfAIChannel();
    virtual ~sis8300llrfAIChannel();

    virtual ndsStatus readNewMAPoint(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    
    /* overriden because unsupported */
    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);

protected:
    static std::string PV_REASON_SIGNAL_ANGLE;
    static std::string PV_REASON_SIGNAL_MAG;
    static std::string PV_REASON_SIGNAL_I;
    static std::string PV_REASON_SIGNAL_Q;
    static std::string PV_REASON_NEW_MA_POINT;

    int _interruptIdSignalAngle;
    int _interruptIdSignalMag;
    int _interruptIdSignalI;
    int _interruptIdSignalQ;
    int _interruptIdNewMAPoint;
    
    virtual ndsStatus onLeaveProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300llrfAIChannel_h */
