/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfModRippleFilChannel.cpp
 * @brief Implementation of sis8300llrf Modulator ripple filter channel in NDS
 * @author urojec
 * @date 23.1.2015
 * 
 * Class that exposes all the settings available for signal monitors. 
 * Signal monitors are available on channels AI2 - AI9. Cavity (AI0) and
 * reference (AI1) input have no signal monitoring capabilities.
 * 
 * The class also reads out pulse-to-pulse data, which includes ALARM, PMS 
 * and interlock (ILOCK) status for the specific channel, current
 * magnitude value and minimum or maximum amplitude in the last monitor active phase.
 * If the alarm is set to trigger (#setMonitorAlaramCondition) over treshold the 
 * maximum amplitude is returned, if it is set to below treshold, than minimum 
 * amplitude is returned.
 * Monitor active phase is defined with monitor start (#setMonitorStartEvent)
 * and monitor stop (#setMonitorStopEvent) events. 
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfChannelGroup.h"
#include "sis8300llrfSignalMonitorChannel.h"

#define SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(retval) {                                \
    if (getChannelNumber() == SIS8300LLRFDRV_AI_CHAN_CAV ||                              \
        getChannelNumber() == SIS8300LLRFDRV_AI_CHAN_REF) {                              \
        NDS_WRN("Signal monitor not available for cavity (AI0) and reference (AI1) input. Ignoring."); \
        return retval;                                                      \
    }                                                                         \
}

std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_SIGMON_ALARM       = "SigmonAlaram";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_SIGMON_PMS         = "SigmonPMS";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_SIGMON_ILOCK       = "SigmonILOCK";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_MAG_MINMAX         = "MagMinMax";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_MAG_CURRENT        = "MagCurrent";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_MAG_TERSHOLD       = "MagTreashold";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_MONITOR_START_EVNT = "MonitorStartEvnt";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_MONITOR_STOP_EVNT  = "MonitorStopEvnt";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_MONITOR_ALARM_COND = "MonitorAlarmCnd";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_MONITOR_PMS_EN     = "MonitorPMSEn";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_MONITOR_ILOCK_EN   = "MonitorILOCKEn";
std::string sis8300llrfSignalMonitorChannel::
                    PV_REASON_SIGNAL_TYPE_DC     = "SignalTypeDC";


/**
 * @brief sis8300llrfSignalMonitorChannel constructor
 */
sis8300llrfSignalMonitorChannel::sis8300llrfSignalMonitorChannel() :
        sis8300llrfChannel(
            SIS8300LLRFDRV_SIGMON_PARAM_NUM, 
            SIS8300LLRFDRV_SIGMON_PARAM_INT_FIRST) {

    sprintf(_ChanStringIdentifier, "Signal Monitoring");


    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfSignalMonitorChannel::onLeaveProcessing, 
        this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfSignalMonitorChannel::onEnterReset, 
        this));

}
/**
 * @brief sis8300llrfSignalMonitorChannel destructor
 */
sis8300llrfSignalMonitorChannel::~sis8300llrfSignalMonitorChannel() {}

/**
 * @brief Signal monitors are not available on AI0 and AI1. So just set
 *        the number of parameters to zero and disable the channels.
 * 
 * @return ndsSuccess   Always
 * 
 * This needs to be done after the channel is registered with the group,
 * becuase we need the channel number.
 */
void sis8300llrfSignalMonitorChannel::onRegistered() {
    NDS_TRC("%s", __func__);

    sis8300llrfChannel::onRegistered();
    
    /* If we have this function than we do not need to override any of 
     * THE parents #commitParameters, #readParameters or 
     * #markAllParametersChanged. We can also disable the channel here. 
     * Generally the disabling of LLRF channels is not allowed */
    
    if (getChannelNumber() == SIS8300LLRFDRV_AI_CHAN_CAV ||
        getChannelNumber() == SIS8300LLRFDRV_AI_CHAN_REF) {
        
        lock();
        _isEnabled = 0;
        _ParamNum  = 0;
        unlock();
    }
}

/**
 * @brief Read out data that has to be updated after every pulse
 *
 * @param [in]  from    From channel state
 * @param [in]  to      To Channels state
 *
 * @return ndsSuccess   Always
 */
ndsStatus sis8300llrfSignalMonitorChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    
    return checkStatuses();
}

/**
 * @see #sis8300llrfChannel::onEnterReset
 */
ndsStatus sis8300llrfSignalMonitorChannel::onEnterReset() {
    
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    return checkStatuses();
}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfSignalMonitorChannel::readParameter(
                int paramIdx, double *paramVal) {
    return sis8300llrfdrv_get_sigmon_param(
            _DeviceUser, (sis8300llrfdrv_sigmon_param) paramIdx, 
            (int) getChannelNumber(), paramVal);
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfSignalMonitorChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_sigmon_param(
                _DeviceUser, (sis8300llrfdrv_sigmon_param) paramIdx, 
                (int) getChannelNumber(), _ParamVals[paramIdx], paramErr);
}

/**
 * @brief All the statuses that need to be checked on leave processing
 *        and enter reset
 */
inline ndsStatus sis8300llrfSignalMonitorChannel::checkStatuses() {
    epicsFloat64 valFloat64;
    epicsInt32   valInt32;
    
    if (getMagMinMax(NULL, &valFloat64) != ndsSuccess) {
        return ndsError;
    }
    doCallbacksFloat64(valFloat64, _interruptIdMagMinMax);
    
    if (getMagCurrent(NULL, &valFloat64) != ndsSuccess) {
        return ndsError;
    }
    doCallbacksFloat64(valFloat64, _interruptIdMagCurrent);
    
    if (getSigmonStatusAlarm(NULL, &valInt32) != ndsSuccess) {
        return ndsError;
    }
    doCallbacksInt32(valInt32, _interruptIdSigmonAlaram);
    
    if (getSigmonStatusPMS(NULL, &valInt32) != ndsSuccess) {
        return ndsError;
    }
    doCallbacksInt32(valInt32, _interruptIdSigmonPMS);
    
    if (getSigmonStatusILOCK(NULL, &valInt32) != ndsSuccess) {
        return ndsError;
    }
    doCallbacksInt32(valInt32, _interrputIdSigmonILOCK);
    
    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfSignalMonitorChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    
    NDS_PV_REGISTER_INT32(
            sis8300llrfSignalMonitorChannel::PV_REASON_SIGMON_ALARM,
            &sis8300llrfSignalMonitorChannel::setInt32,
            &sis8300llrfSignalMonitorChannel::getSigmonStatusAlarm,
            &_interruptIdSigmonAlaram);

    NDS_PV_REGISTER_INT32(
            sis8300llrfSignalMonitorChannel::PV_REASON_SIGMON_PMS,
            &sis8300llrfSignalMonitorChannel::setInt32,
            &sis8300llrfSignalMonitorChannel::getSigmonStatusPMS,
            &_interruptIdSigmonPMS);

    NDS_PV_REGISTER_INT32(
            sis8300llrfSignalMonitorChannel::PV_REASON_SIGMON_ILOCK,
            &sis8300llrfSignalMonitorChannel::setInt32,
            &sis8300llrfSignalMonitorChannel::getSigmonStatusILOCK,
            &_interrputIdSigmonILOCK);
            
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfSignalMonitorChannel::PV_REASON_MAG_MINMAX,
            &sis8300llrfSignalMonitorChannel::setFloat64,
            &sis8300llrfSignalMonitorChannel::getMagMinMax,
            &_interruptIdMagMinMax);
    
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfSignalMonitorChannel::PV_REASON_MAG_CURRENT,
            &sis8300llrfSignalMonitorChannel::setFloat64,
            &sis8300llrfSignalMonitorChannel::getMagCurrent,
            &_interruptIdMagCurrent);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfSignalMonitorChannel::PV_REASON_MAG_TERSHOLD,
            &sis8300llrfSignalMonitorChannel::setMagTreshold,
            &sis8300llrfSignalMonitorChannel::getMagTreshold,
            &_interruptIds[sigmon_treshold]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfSignalMonitorChannel::PV_REASON_MONITOR_START_EVNT,
            &sis8300llrfSignalMonitorChannel::setMonitorStartEvent,
            &sis8300llrfSignalMonitorChannel::getMonitorStartEvent,
            &_interruptIds[sigmon_start_evnt]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfSignalMonitorChannel::PV_REASON_MONITOR_STOP_EVNT,
            &sis8300llrfSignalMonitorChannel::setMonitorStopEvent,
            &sis8300llrfSignalMonitorChannel::getMonitorStopEvent,
            &_interruptIds[sigmon_end_evnt]);

    NDS_PV_REGISTER_INT32(sis8300llrfSignalMonitorChannel::PV_REASON_MONITOR_ALARM_COND,
            &sis8300llrfSignalMonitorChannel::setMonitorAlaramCondition,
            &sis8300llrfSignalMonitorChannel::getMonitorAlaramCondition,
            &_interruptIds[sigmon_alarm_cnd]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfSignalMonitorChannel::PV_REASON_MONITOR_PMS_EN,
            &sis8300llrfSignalMonitorChannel::setMonitorPMSEnabled,
            &sis8300llrfSignalMonitorChannel::getMonitorPMSEnabled,
            &_interruptIds[sigmon_pms_en]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfSignalMonitorChannel::PV_REASON_MONITOR_ILOCK_EN,
            &sis8300llrfSignalMonitorChannel::setMonitorILOCKEnabled,
            &sis8300llrfSignalMonitorChannel::getMonitorILOCKEnabled,
            &_interruptIds[sigmon_ilock_en]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfSignalMonitorChannel::PV_REASON_SIGNAL_TYPE_DC,
            &sis8300llrfSignalMonitorChannel::setSignalTypeDC,
            &sis8300llrfSignalMonitorChannel::getSignalTypeDC,
            &_interruptIds[sigmon_dc]);

    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/* ==== SIGNAL MONITOR STATUS READOUTS FROM HARDWARE ==== */

/**
 * @brief Check if signal monitor alarm was raised on this channel.
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success this will be 1 if alarm is active 
 *                          and 0 if not
 * 
 * @return ndsSuccess   Data retrieved successfully
 * @return ndsError     For AI0 and AI1, during IOC INIT or if data could
 *                      not be read from the device
 *        
 * Alarm can trigger below or over magnitude treshold. Alarm condition 
 * is settable with #setMonitorAlaramCondition and the magnitude 
 * treshold with #setMagTreshold. Alarm will only be raised during 
 * monitor active phase, which is defined with start 
 * (#setMonitorStartEvent) and stop (#setMonitorStopEvent) event.
 */
ndsStatus sis8300llrfSignalMonitorChannel::getSigmonStatusAlarm(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    
    int status;
    unsigned uRegVal;
    unsigned shift;
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    status = sis8300llrfdrv_get_sigmon_status(
                _DeviceUser, sigmon_stat_alarm, &uRegVal);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_sigmon_status", status);
    
    shift = (unsigned) getChannelNumber() - 
                (unsigned) SIS8300LLRFDRV_SIGMON_CHAN_FIRST;
    uRegVal &= 0x1 << shift;
    uRegVal >>= shift;
    
    *value = (epicsInt32) uRegVal;

    return ndsSuccess;
}
/**
 * @brief Check if PMS was raised on this channel.
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success this will be 1 if PMS is active 
 *                          and 0 if not
 * 
 * @return ndsSuccess   Data retrieved successfully
 * @return ndsError     For AI0 and AI1, during IOC INIT or if data could
 *                      not be read from the device
 * 
 * PMS will trigger if alarm was raised for this channel 
 * (#getSigmonStatusAlarm) and if PMS was enabled (#setMonitorPMSEnabled)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getSigmonStatusPMS(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    
    int status;
    unsigned uRegVal;
    unsigned shift;
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    status = sis8300llrfdrv_get_sigmon_status(
                _DeviceUser, sigmon_stat_pms, &uRegVal);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_sigmon_status", status);
    
    shift = (unsigned) getChannelNumber() - 
                (unsigned) SIS8300LLRFDRV_SIGMON_CHAN_FIRST;
    uRegVal &= 0x1 << shift;
    uRegVal >>= shift;
    
    *value = (epicsInt32) uRegVal;

    return ndsSuccess;
}
/**
 * @brief Check if interlock was raised on this channel.
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success this will be 1 if interlock is 
 *                          active and 0 if not
 * 
 * @return ndsSuccess   Data retrieved successfully
 * @return ndsError     For AI0 and AI1, during IOC INIT or if data could
 *                      not be read from the device
 * 
 * PMS will trigger if alarm was raised for this channel 
 * (#getSigmonStatusAlarm) and if PMS was enabled 
 * (#setMonitorILOCKEnabled)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getSigmonStatusILOCK(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    
    int status;
    unsigned uRegVal;
    unsigned shift;
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    status = sis8300llrfdrv_get_sigmon_status(
                _DeviceUser, sigmon_stat_ilock, &uRegVal);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_sigmon_status", status);
    
    shift = (unsigned) getChannelNumber() - 
                (unsigned) SIS8300LLRFDRV_SIGMON_CHAN_FIRST;
    uRegVal &= 0x1 << shift;
    uRegVal >>= shift;
    
    *value = (epicsInt32) uRegVal;

    return ndsSuccess;
}
/**
 * @brief Get the current magnitude value for this channel
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If tis is channel AI0 or AI1, if we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfSignalMonitorChannel::getMagCurrent(
                asynUser *pasynUser,  epicsFloat64 *value) {
    double doubleVal;
    int status; 
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);
    
    status = sis8300llrfdrv_get_sigmon_mag_curr(
                _DeviceUser, (int) getChannelNumber(), &doubleVal);
    SIS8300NDS_STATUS_CHECK(
        "sis8300llrfdrv_get_sigmon_mag_curr", status);
    
    *value = (epicsFloat64) doubleVal;
    
    return ndsSuccess;
}
/**
 * @brief Get the minimum or maximum magnitude for this channel during  
 *        the last signal monitor active period.
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the min or max magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If tis is channel AI0 or AI1, if we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 * 
 * This will return the minimum magnitude if the signal monitor is set 
 * to trigger below treshold and maximum magnitude if it is set to 
 * trigger below treshold (#setMonitorAlaramCondition).
 */
ndsStatus sis8300llrfSignalMonitorChannel::getMagMinMax(
                asynUser *pasynUser,  epicsFloat64 *value) {
    double doubleVal;
    int status; 
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);
    
    status = sis8300llrfdrv_get_sigmon_mag_minmax(
                _DeviceUser, (int) getChannelNumber(), &doubleVal);
    SIS8300NDS_STATUS_CHECK(
        "sis8300llrfdrv_get_sigmon_mag_minmax", status);
    
    *value = (epicsFloat64) doubleVal;
    
    return ndsSuccess;
}


/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ========== RELATED TO SIGNAL MONITORING =========== */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set magnitude treshold value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the magnitude treshold to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfSignalMonitorChannel::setMagTreshold(
                asynUser *pasynUser,  epicsFloat64 value) {
    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    _ParamVals[sigmon_treshold] = (double) value;
    _ParamChanges[sigmon_treshold] = 1;

    return commitParameters();
}
/**
 * @brief Get signal monitor magnitude treshold value
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the treshold value on success
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings or if signal monitor is not 
 *                     available on this channel (AI0 and AI1).
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getMagTreshold(
                asynUser *pasynUser,  epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    *value = (epicsFloat64) _ParamVals[sigmon_treshold];
    return ndsSuccess;
}
/**
 * @brief Set signal monitor start event. It defines the begining of
 *        monitor active phase.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set to 0 = PULSE_COMMING, 1 = PULSE_START, 
 *                          2 = PULSE_END, 3 = NEVER
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfSignalMonitorChannel::setMonitorStartEvent(
                asynUser *pasynUser, epicsInt32 value) {
    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    _ParamVals[sigmon_start_evnt] = (double) value;
    _ParamChanges[sigmon_start_evnt] = 1;

    return commitParameters();
}
/**
 * @brief Get signal monitor start event
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the event number on success, 
 *                          0 = PULSE_COMMING, 1 = PULSE_START, 
 *                          2 = PULSE_END, 3 = NEVER
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings or if signal monitor is not 
 *                     available on this channel (AI0 and AI1).
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getMonitorStartEvent(
                asynUser *pasynUser, epicsInt32 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    *value = (epicsInt32) _ParamVals[sigmon_start_evnt];
    return ndsSuccess;
}
/**
 * @brief Set signal monitor stop event. It defines the end of
 *        monitor active phase.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set to 1 = PULSE_START, 2 = PULSE_END, 
 *                          3 = NEVER
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfSignalMonitorChannel::setMonitorStopEvent(
                asynUser *pasynUser, epicsInt32 value) {
    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    _ParamVals[sigmon_end_evnt] = (double) value;
    _ParamChanges[sigmon_end_evnt] = 1;

    return commitParameters();
}
/**
 * @brief Get signal monitor stop event
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the event number on success, 
 *                          1 = PULSE_START, 2 = PULSE_END, 3 = NEVER
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings or if signal monitor is not 
 *                     available on this channel (AI0 and AI1).
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getMonitorStopEvent(
                asynUser *pasynUser, epicsInt32 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    *value = (epicsInt32) _ParamVals[sigmon_end_evnt];
    return ndsSuccess;
}
/**
 * @brief Set signal monitor alarm condition. The alarm can be set to  
 *        trigger below or above magnitude treshold (settable 
 *        with #setMagTreshold)
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set to 1 = TRIGGER ABOVE, 0 = TRIGGER BELOW
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfSignalMonitorChannel::setMonitorAlaramCondition(
                asynUser *pasynUser, epicsInt32 value) {
    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    _ParamVals[sigmon_alarm_cnd] = (double) value;
    _ParamChanges[sigmon_alarm_cnd] = 1;

    return commitParameters();
}
/**
 * @brief Check weather the monitor alarm is set to trigger below
 *        or above treshold (settable with #setMagTreshold)
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success this will be 1 = TRIGGER ABOVE, 
 *                          0 = TRIGGER BELOW
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings or if signal monitor is not 
 *                     available on this channel (AI0 and AI1).
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getMonitorAlaramCondition(
                asynUser *pasynUser, epicsInt32 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    *value = (epicsInt32) _ParamVals[sigmon_alarm_cnd];
    return ndsSuccess;
}
/**
 * @brief Enable triggering of PMS when signal monitor alarm for 
 *        this channel triggers
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfSignalMonitorChannel::setMonitorPMSEnabled(
                asynUser *pasynUser, epicsInt32 value) {
    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    _ParamVals[sigmon_pms_en] = (double) value;
    _ParamChanges[sigmon_pms_en] = 1;

    return commitParameters();
}
/**
 * @brief Check if signal monitor alarm on this channel will trigger PMS
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings or if signal monitor is not 
 *                     available on this channel (AI0 and AI1).
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getMonitorPMSEnabled(
                asynUser *pasynUser, epicsInt32 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    *value = (epicsInt32) _ParamVals[sigmon_pms_en];
    return ndsSuccess;
}
/**
 * @brief Enable triggering of interlock when signal monitor alarm for 
 *        this channel triggers
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfSignalMonitorChannel::setMonitorILOCKEnabled(
                asynUser *pasynUser, epicsInt32 value) {
    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    _ParamVals[sigmon_ilock_en] = (double) value;
    _ParamChanges[sigmon_ilock_en] = 1;

    return commitParameters();
}
/**
 * @brief Check if signal monitor alarm on this channel will trigger 
 *        interlock
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings or if signal monitor is not 
 *                     available on this channel (AI0 and AI1).
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getMonitorILOCKEnabled(
                asynUser *pasynUser, epicsInt32 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    *value = (epicsInt32) _ParamVals[sigmon_ilock_en];
    return ndsSuccess;
}
/**
 * @brief Set if this signal is AC or DC
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set to 1 = DC, 0 = AC
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfSignalMonitorChannel::setSignalTypeDC(
                asynUser *pasynUser, epicsInt32 value) {
    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    _ParamVals[sigmon_dc] = (double) value;
    _ParamChanges[sigmon_dc] = 1;

    return commitParameters();
}
/**
 * @brief Check if this signal is set to be DC
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success this will be 1 = DC, 0 = AC
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings or if signal monitor is not 
 *                     available on this channel (AI0 and AI1).
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfSignalMonitorChannel::getSignalTypeDC(
                asynUser *pasynUser, epicsInt32 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    SIS8300LLRFNDS_IS_SIGMON_AVAILABLE(ndsError);

    *value = (epicsInt32) _ParamVals[sigmon_dc];
    return ndsSuccess;
}
