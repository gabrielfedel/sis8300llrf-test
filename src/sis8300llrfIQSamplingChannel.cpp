/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfIOControlChannel.cpp
 * @brief Implementation of LLRF Input-Output control channel in NDS.
 * @author urojec
 * @date 2.6.2014
 * 
 * This class contains all the non IQ sampling settings available on
 * the device.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"

#include "sis8300llrfIQSamplingChannel.h"

std::string sis8300llrfIQSamplingChannel::
                PV_REASON_IQ_ANGLE_OFFSET          = "IQAngleOffset";
std::string sis8300llrfIQSamplingChannel::
                PV_REASON_IQ_ANGLE_OFFSET_EN       = "IQAngleOffsetEn";
std::string sis8300llrfIQSamplingChannel::
                PV_REASON_IQ_CAVITY_INPUT_DELAY    = "IQCavInpDelay";
std::string sis8300llrfIQSamplingChannel::
                PV_REASON_IQ_CAVITY_INPUT_DELAY_EN = "IQCavInpDelayEn";
std::string sis8300llrfIQSamplingChannel::
                PV_REASON_NEAR_IQ_PARAM_M          = "NearIqParamM";
std::string sis8300llrfIQSamplingChannel::
                PV_REASON_NEAR_IQ_PARAM_N          = "NearIqParamN";

/**
 * @brief IQ Sampling Channel constructor.
 */
sis8300llrfIQSamplingChannel::sis8300llrfIQSamplingChannel()
    : sis8300llrfChannel (
            SIS8300LLRFDRV_IQ_PARAM_NUM, 
            SIS8300LLRFDRV_IQ_PARAM_INT_FIRST) {

    strcpy(_ChanStringIdentifier, "IQ channel");
}

/**
 * @brief IQ Sampling channel destructor
 */
sis8300llrfIQSamplingChannel::~sis8300llrfIQSamplingChannel() {}


/**
 * @see #sis8300llrfChannel::writeToHardware
 */
ndsStatus sis8300llrfIQSamplingChannel::writeToHardware(){
    NDS_TRC("%s", __func__);

    int status;
    unsigned paramRbvM, paramRbvN;
    
    sis8300llrfControllerChannelGroup *cg = 
            dynamic_cast<sis8300llrfControllerChannelGroup*> (getChannelGroup());

    if (_NearIqParamMChanged || _NearIqParamNChanged) {
        status = sis8300llrfdrv_set_near_iq(
                    _DeviceUser, _NearIqParamM, _NearIqParamN);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_near_iq", status);

        status = sis8300llrfdrv_get_near_iq(
                    _DeviceUser, &paramRbvM, &paramRbvN);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_get_near_iq", status);

        if (paramRbvM != _NearIqParamM || paramRbvN != _NearIqParamN) {
            SIS8300NDS_MSGERR("Val write != val read");
            NDS_ERR("Write val != read val, M:%u/%u, N:%u/%u", 
                paramRbvM, _NearIqParamM, paramRbvN, _NearIqParamN);
            error();
            return ndsError;
        }

        doCallbacksInt32(
            (epicsInt32)_NearIqParamM, _interruptIdNearIqParamM);
        doCallbacksInt32(
            (epicsInt32)_NearIqParamN, _interruptIdNearIqParamN);

        if (_NearIqParamNChanged == 1)
            cg->setN(_NearIqParamN);
        _NearIqParamMChanged = 0;
        _NearIqParamNChanged = 0;

        //cg->lock();
        cg->setUpdateReason(SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS);
        //cg->unlock();
    }

    return sis8300llrfChannel::writeToHardware();
}

/**
 * @see #sis8300llrfChannel::readParameters
 */
ndsStatus sis8300llrfIQSamplingChannel::readParameters(){

    int status;
    unsigned uRegValM, uRegValN;

    status = sis8300llrfdrv_get_near_iq(
                _DeviceUser, &uRegValM, &uRegValN);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_get_io_control_near_iq", status);

    doCallbacksInt32((epicsInt32)uRegValM, _interruptIdNearIqParamM);
    doCallbacksInt32((epicsInt32)uRegValN, _interruptIdNearIqParamN);

    return sis8300llrfChannel::readParameters();
}

/**
 * @see #sis8300llrfChannel::markAllParametersChanged
 */
ndsStatus sis8300llrfIQSamplingChannel::markAllParametersChanged() {
    NDS_TRC("%s", __func__);

    _NearIqParamMChanged = 1;
    _NearIqParamNChanged = 1;

    return sis8300llrfChannel::markAllParametersChanged();
}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfIQSamplingChannel::readParameter(
                int paramIdx, double *paramVal) {
    return sis8300llrfdrv_get_iq_param(_DeviceUser,
            (sis8300llrfdrv_iq_param) paramIdx, paramVal);
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfIQSamplingChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_iq_param(
                _DeviceUser, (sis8300llrfdrv_iq_param) paramIdx, 
                _ParamVals[paramIdx], paramErr);
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfIQSamplingChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfIQSamplingChannel::PV_REASON_IQ_ANGLE_OFFSET,
            &sis8300llrfIQSamplingChannel::setIQAngleOffset,
            &sis8300llrfIQSamplingChannel::getIQAngleOffset,
            &_interruptIds[iq_param_angle_offset_val]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfIQSamplingChannel::PV_REASON_IQ_ANGLE_OFFSET_EN,
            &sis8300llrfIQSamplingChannel::setIQAngleOffsetEnabled,
            &sis8300llrfIQSamplingChannel::getIQAngleOffsetEnabled,
            &_interruptIds[iq_param_angle_offset_en]);
            
    NDS_PV_REGISTER_INT32(
            sis8300llrfIQSamplingChannel::PV_REASON_IQ_CAVITY_INPUT_DELAY,
            &sis8300llrfIQSamplingChannel::setIQCavityInputDelay,
            &sis8300llrfIQSamplingChannel::getIQCavityInputDelay,
            &_interruptIds[iq_param_cav_inp_delay_val]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfIQSamplingChannel::PV_REASON_IQ_CAVITY_INPUT_DELAY_EN,
            &sis8300llrfIQSamplingChannel::setIQCavityInputDelayEnabled,
            &sis8300llrfIQSamplingChannel::getIQCavityInputDelayEnabled,
            &_interruptIds[iq_param_cav_inp_delay_en]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfIQSamplingChannel::PV_REASON_NEAR_IQ_PARAM_M,
            &sis8300llrfIQSamplingChannel::setNearIqParamM,
            &sis8300llrfIQSamplingChannel::getNearIqParamM,
            &_interruptIdNearIqParamM);

    NDS_PV_REGISTER_INT32(
            sis8300llrfIQSamplingChannel::PV_REASON_NEAR_IQ_PARAM_N,
            &sis8300llrfIQSamplingChannel::setNearIqParamN,
            &sis8300llrfIQSamplingChannel::getNearIqParamN,
            &_interruptIdNearIqParamN);

    return ADIOChannel::registerHandlers(pvContainers);
}

/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* =========== RELATED TO NON-IQ SAMPLING ============ */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set the IQ angle offset for near IQ sampling
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Angle offset value to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfIQSamplingChannel::setIQAngleOffset(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[iq_param_angle_offset_val] = (double) value;

    _ParamChanges[iq_param_angle_offset_val] = 1;
    return commitParameters();
}
/**
 * @brief Get the near IQ sampling Angle offset
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold IQ angle offset on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfIQSamplingChannel::getIQAngleOffset(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[iq_param_angle_offset_val];

    return ndsSuccess;
}
/**
 * @brief Eanble IQ angle offset (take the value from #setIQAngleOffset
 *        into account when sampling).
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfIQSamplingChannel::setIQAngleOffsetEnabled(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[iq_param_angle_offset_en] = (double) value;

    _ParamChanges[iq_param_angle_offset_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if angle offset is enabled or not
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfIQSamplingChannel::getIQAngleOffsetEnabled(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[iq_param_angle_offset_en];

    return ndsSuccess;
}
/**
 * @brief Set input delay for cavity signal
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        The delay to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfIQSamplingChannel::setIQCavityInputDelay(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[iq_param_cav_inp_delay_val] = (double) value;

    _ParamChanges[iq_param_cav_inp_delay_val] = 1;
    return commitParameters();
}
/**
 * @brief Get the current cavity input delay value
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       will contain the delay on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfIQSamplingChannel::getIQCavityInputDelay(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[iq_param_cav_inp_delay_val];

    return ndsSuccess;
}
/**
 * @brief Eanble cavity input delay (take the value from 
 *        #setIQCavityInputDelay into account when sampling).
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfIQSamplingChannel::setIQCavityInputDelayEnabled(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[iq_param_cav_inp_delay_en] = (double) value;

    _ParamChanges[iq_param_cav_inp_delay_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if cavity input delay is enabled or not
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfIQSamplingChannel::getIQCavityInputDelayEnabled(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[iq_param_cav_inp_delay_en];

    return ndsSuccess;
}
/**
 * @brief Set near IQ sampling parameter M
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        The M value to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfIQSamplingChannel::setNearIqParamM(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _NearIqParamM = (unsigned) value;

    _NearIqParamMChanged = 1;
    return commitParameters();
}
/**
 * @brief Get the current IQ sampling parameter M
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       will contain the M value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfIQSamplingChannel::getNearIqParamM(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _NearIqParamM;
    return ndsSuccess;
}
/**
 * @brief Set near IQ sampling parameter N
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        The N value to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfIQSamplingChannel::setNearIqParamN(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _NearIqParamN = (unsigned) value;

    _NearIqParamNChanged = 1;
    return commitParameters();
}
/**
 * @brief Get the current IQ sampling parameter N
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       will contain the N value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfIQSamplingChannel::getNearIqParamN(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _NearIqParamN;
    return ndsSuccess;
}
