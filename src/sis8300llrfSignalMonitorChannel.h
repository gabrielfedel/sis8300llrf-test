/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfSignalMonitorChannel.h
 * @brief Header file defining the LLRF Modulator ripple filter class
 * @author urojec, ursa.rojec@cosylab.com
 * @date 23.1.2015
 */

#ifndef _sis8300llrfSignalMonitorChannel_h
#define _sis8300llrfSignalMonitorChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief Modulator filter specific implementation of
 *        @see #sis8300llrfChannel Class
 */
class sis8300llrfSignalMonitorChannel: public sis8300llrfChannel {
public:
    sis8300llrfSignalMonitorChannel();
    virtual ~sis8300llrfSignalMonitorChannel();
    
    inline ndsStatus checkStatuses();
    
    virtual void onRegistered();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setMagTreshold(
                        asynUser *pasynUser,  epicsFloat64 value);
    virtual ndsStatus getMagTreshold(
                        asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getMagMinMax(
                        asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getMagCurrent(
                        asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus setMonitorStartEvent(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getMonitorStartEvent(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setMonitorStopEvent(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getMonitorStopEvent(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setMonitorAlaramCondition(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getMonitorAlaramCondition(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setMonitorPMSEnabled(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getMonitorPMSEnabled(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setMonitorILOCKEnabled(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getMonitorILOCKEnabled(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setSignalTypeDC(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getSignalTypeDC(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getSigmonStatusAlarm(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getSigmonStatusPMS(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getSigmonStatusILOCK(
                        asynUser *pasynUser, epicsInt32 *value);
    
protected:
    
    /* for asynReasons */
    static std::string PV_REASON_MAG_TERSHOLD;
    static std::string PV_REASON_MONITOR_START_EVNT;
    static std::string PV_REASON_MONITOR_STOP_EVNT;
    static std::string PV_REASON_MONITOR_ALARM_COND;
    static std::string PV_REASON_MONITOR_PMS_EN;
    static std::string PV_REASON_MONITOR_ILOCK_EN;
    static std::string PV_REASON_SIGNAL_TYPE_DC;
    
    static std::string PV_REASON_MAG_MINMAX;
    static std::string PV_REASON_MAG_CURRENT;
    
    static std::string PV_REASON_SIGMON_ALARM;
    static std::string PV_REASON_SIGMON_PMS;
    static std::string PV_REASON_SIGMON_ILOCK;

    int _interruptIdSigmonAlaram;
    int _interruptIdSigmonPMS;
    int _interrputIdSigmonILOCK;
    
    int _interruptIdMagMinMax;
    int _interruptIdMagCurrent;

    /* parameter read/write */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);

    /* state transitions */
    virtual ndsStatus onEnterReset();
    virtual ndsStatus onLeaveProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300llrfSignalMonitorChannel_h */

