/*
 * m-epics-sis8300llrf
 * Copyright (C) 2016

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrNotchFilterChannel.h
 * @brief Header file defining the LLRF Notch filter class.
 * @author asoderqvist, alexander.soderqvist@cosylab.com
 * @date 4.3.2016
 */

#ifndef _sis8300llrfNotchFilterChannel_h
#define _sis8300llrfNotchFilterChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief Notch Filter implementation of
 *        @see #sis8300llrfChannel Class
 */
class sis8300llrfNotchFilterChannel: public sis8300llrfChannel {
public:
    sis8300llrfNotchFilterChannel(epicsFloat64 FSampling);
    virtual ~sis8300llrfNotchFilterChannel();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus setConstantAReal(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantAReal(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setConstantAImag(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantAImag(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setConstantBReal(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantBReal(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setConstantBImag(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantBImag(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getEnable(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setFrequency(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getFrequency(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setBandwidth(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getBandwidth(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus commitParameters();
    virtual ndsStatus updateFilter();


protected:

    /* for asynReasons */
    static std::string PV_REASON_CONST_A_REAL;
    static std::string PV_REASON_CONST_A_IMAG;
    static std::string PV_REASON_CONST_B_REAL;
    static std::string PV_REASON_CONST_B_IMAG;
    static std::string PV_REASON_FREQ;
    static std::string PV_REASON_BWIDTH;
    static std::string PV_REASON_EN;

    int _interruptIdFreq;
    int _interruptIdBwidth;

    epicsFloat64 _Frequency;
    epicsFloat64 _Bandwidth;

    epicsFloat64 _FSampling;

    epicsInt32  _FrequencyChanged;
    epicsInt32  _BandwidthChanged;

    /* parameter read/write */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);
};

#endif /* _sis8300llrfNotchFilterChannel_h */

