/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfChannelGroup.h
 * @brief Header file defining the generic LLRF ChannelGroup class
 * @author urojec
 * @date 6.5.2014
 */

#ifndef _sis8300llrfChannelGroup_h
#define _sis8300llrfChannelGroup_h

#include <ndsChannelGroup.h>


/**
 * @brief sis8300 llrf specific nds::ChannelGroup class that is the
 *           base for all other llrf channel groups
 *
 * Function of this class is very similar to @see #sis8300llrfChannel.h
 * It registers common handlers that correspond to llrf controller state machine,
 * provides functions for tracking parameter changes and setting the device user
 */
class sis8300llrfChannelGroup: public nds::ChannelGroup {

public:
    sis8300llrfChannelGroup(const std::string& name, sis8300drv_usr *newDeviceUser);
    virtual ~sis8300llrfChannelGroup();

    virtual ndsStatus initialize();
    sis8300drv_usr* getDeviceUser();

    virtual unsigned getUpdateReason();
    virtual void setUpdateReason(unsigned reason);

    virtual ndsStatus commitParameters();
    virtual ndsStatus forceCommitParameters();
    virtual ndsStatus readParameters();
    virtual ndsStatus markAllParametersChanged();

    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus getSamplesCount(asynUser *pasynUser, epicsInt32 *value);
   
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    /* Overriden because unsupported */
    virtual ndsStatus handleOnMsg(
                        asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleOffMsg(
                        asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleResetMsg(
                        asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleStartMsg(
                        asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleStopMsg(
                        asynUser *pasynUser, const nds::Message& value);

protected:
    sis8300drv_usr *_DeviceUser; /**< User context for sis8300drv, 
                                      set by NDS Device. */
    unsigned _Initialized;       /**< Will get set to 1 once the group
                                   *  is initialized. This is used 
                                   *  because the file descriptor is not 
                                   *  yet available at iocInit to get 
                                   *  the data from device */
    unsigned _UpdateReason;      /**< Will be used by 
                                   * #sis8300llrfDevice::_PulseSetupTask 
                                   * to determine the update reason for 
                                   * the controller */
                                      
    /* write data */
    virtual ndsStatus writeToHardware();

    /* State change hanlers, define them here because all 
     * the child classes will need them. */
    virtual ndsStatus onEnterProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterDisabled(
                        nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterReset();
    virtual ndsStatus resetChannels();
    virtual ndsStatus disableChannels();

    static std::string PV_REASON_CH_DATA_READY;

    int _interruptIdChannelDataReady;

};

#endif /* _sis8300llrfChannelGroup_h */
