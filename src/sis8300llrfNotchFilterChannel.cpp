/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @file sis8300llrNotchFilterChannel.cpp
 * @brief Implementation of sis8300llrf filter channel in NDS
 * @author asoderqvist
 * @date 4.3.2015
 * 
 * This class holds all the settings related to the notch filter that are 
 * available on the device. It is more or less a collection of setters and
 * getters, since this unit has no readouts that have to be made on 
 * pulse-to-pulse basis.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"
#include "sis8300llrfNotchFilterChannel.h"

#include <math.h>

std::string sis8300llrfNotchFilterChannel::
                    PV_REASON_CONST_A_REAL    = "NotchFilConstAReal";
std::string sis8300llrfNotchFilterChannel::
                    PV_REASON_CONST_A_IMAG    = "NotchFilConstAImag";
std::string sis8300llrfNotchFilterChannel::
                    PV_REASON_CONST_B_REAL    = "NotchFilConstBReal";
std::string sis8300llrfNotchFilterChannel::
                    PV_REASON_CONST_B_IMAG    = "NotchFilConstBImag";
std::string sis8300llrfNotchFilterChannel::
                    PV_REASON_EN              = "NotchFilEn";
std::string sis8300llrfNotchFilterChannel::
                    PV_REASON_FREQ            = "NotchFilFreq";
std::string sis8300llrfNotchFilterChannel::
                    PV_REASON_BWIDTH          = "NotchFilBwidth";


/**
 * @brief sis8300llrfNotchFilterChannel constructor
 */
sis8300llrfNotchFilterChannel::sis8300llrfNotchFilterChannel(epicsFloat64 FSampling) :
        sis8300llrfChannel(
            SIS8300LLRFDRV_NOTCH_FILTER_PARAM_NUM, 
            SIS8300LLRFDRV_NOTCH_FILTER_PARAM_INT_FIRST) {

    _Frequency = 0;
    _Bandwidth = 0;
    _FrequencyChanged = 0;
    _BandwidthChanged = 0;
    _FSampling = FSampling;

    sprintf(_ChanStringIdentifier, "Notch filter");
}


/**
 * @brief sis8300llrfNotchFilterChannel destructor
 */
sis8300llrfNotchFilterChannel::~sis8300llrfNotchFilterChannel() {}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfNotchFilterChannel::readParameter(
                int paramIdx, double *paramVal) {
    return sis8300llrfdrv_get_notch_filter_param(_DeviceUser,
            (sis8300llrfdrv_notch_filter_param) paramIdx, paramVal);
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfNotchFilterChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_notch_filter_param(_DeviceUser,
                (sis8300llrfdrv_notch_filter_param) paramIdx, 
                _ParamVals[paramIdx], paramErr);
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfNotchFilterChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfNotchFilterChannel::PV_REASON_CONST_A_REAL,
            &sis8300llrfNotchFilterChannel::setConstantAReal,
            &sis8300llrfNotchFilterChannel::getConstantAReal,
            &_interruptIds[notch_fil_const_a_real]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfNotchFilterChannel::PV_REASON_CONST_A_IMAG,
            &sis8300llrfNotchFilterChannel::setConstantAImag,
            &sis8300llrfNotchFilterChannel::getConstantAImag,
            &_interruptIds[notch_fil_const_a_imag]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfNotchFilterChannel::PV_REASON_CONST_B_REAL,
            &sis8300llrfNotchFilterChannel::setConstantBReal,
            &sis8300llrfNotchFilterChannel::getConstantBReal,
            &_interruptIds[notch_fil_const_b_real]);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfNotchFilterChannel::PV_REASON_CONST_B_IMAG,
           &sis8300llrfNotchFilterChannel::setConstantBImag,
           &sis8300llrfNotchFilterChannel::getConstantBImag,
           &_interruptIds[notch_fil_const_b_imag]);

    NDS_PV_REGISTER_INT32(
           sis8300llrfNotchFilterChannel::PV_REASON_EN,
           &sis8300llrfNotchFilterChannel::setEnable,
           &sis8300llrfNotchFilterChannel::getEnable,
           &_interruptIds[notch_fil_en]);

   NDS_PV_REGISTER_FLOAT64(
            sis8300llrfNotchFilterChannel::PV_REASON_FREQ,
            &sis8300llrfNotchFilterChannel::setFrequency,
            &sis8300llrfNotchFilterChannel::getFrequency,
            &_interruptIdFreq);

   NDS_PV_REGISTER_FLOAT64(
            sis8300llrfNotchFilterChannel::PV_REASON_BWIDTH,
            &sis8300llrfNotchFilterChannel::setBandwidth,
            &sis8300llrfNotchFilterChannel::getBandwidth,
            &_interruptIdBwidth);

    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ======= RELATED TO NOTCH FILTER ======== */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set Notch filter constant A Real part
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the real part of the A constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfNotchFilterChannel::setConstantAReal(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[notch_fil_const_a_real] = (double) value;

    _ParamChanges[notch_fil_const_a_real] = 1;
    return commitParameters();
}
/**
 * @brief Get notch filter constant A real part
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the real part of A constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfNotchFilterChannel::getConstantAReal(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[notch_fil_const_a_real];

    return ndsSuccess;
}
/**
 * @brief Set notch filter constant A imaginary part
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the imaginary part of the B constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfNotchFilterChannel::setConstantAImag(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[notch_fil_const_a_imag] = (double) value;

    _ParamChanges[notch_fil_const_a_imag] = 1;
    return commitParameters();
}
/**
 * @brief Get notch filter constant A imag part
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the imag part of the A constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfNotchFilterChannel::getConstantAImag(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[notch_fil_const_a_imag];

    return ndsSuccess;
}
/**
 * @brief Set notch filter constant B real part
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the real part of the B constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfNotchFilterChannel::setConstantBReal(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[notch_fil_const_b_real] = (double) value;

    _ParamChanges[notch_fil_const_b_real] = 1;
    return commitParameters();
}
/**
 * @brief Get modulator filter constant B real part
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the real part of the B constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfNotchFilterChannel::getConstantBReal(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[notch_fil_const_b_real];

    return ndsSuccess;
}
/**
 * @brief Set notch filter constant B imag part
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the imaginary part of the B constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfNotchFilterChannel::setConstantBImag(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[notch_fil_const_b_imag] = (double) value;

    _ParamChanges[notch_fil_const_b_imag] = 1;
    return commitParameters();
}
/**
 * @brief Get modulator filter constant B imaginary part
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the imaginary part of the B constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfNotchFilterChannel::getConstantBImag(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[notch_fil_const_b_imag];

    return ndsSuccess;
}

/**
 * @brief Enable notch filter
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfNotchFilterChannel::setEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[notch_fil_en] = (double) value;

    _ParamChanges[notch_fil_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if notch flter is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfNotchFilterChannel::getEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[notch_fil_en];

    return ndsSuccess;
}


/**
 * @brief Set frequency for Notch Filter
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Frequency value
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfNotchFilterChannel::setFrequency(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);
    
    _Frequency = value;
    _FrequencyChanged = 1;

    return commitParameters();
}


/**
 * @brief Get Frequency
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Frequency value 
 *                          
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfNotchFilterChannel::getFrequency(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _Frequency;

    return ndsSuccess;
}

/**
 * @brief Set Bandwidth for Notch Filter
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Bandwidth value
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfNotchFilterChannel::setBandwidth(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _Bandwidth = value;
    _BandwidthChanged = 1;

    return commitParameters();
}


/**
 * @brief GetBandwidth 
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Bandwidth value 
 *                          
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfNotchFilterChannel::getBandwidth(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _Bandwidth;

    return ndsSuccess;
}

/**
 * @brief Overides commitParameters to manage Frequency and
 * Bandwidth
 * 
 * @return ndsError    If update or commitParamters return error
 * @return ndsSuccess  
 * 
 * This method is necessary so Frequency and Bandwidth is managed differently
 * from other PVs. These PVs don't have a register to write, instead of they 
 * will set constants a and b.
 */
ndsStatus sis8300llrfNotchFilterChannel::commitParameters() {
    ndsStatus status;

    NDS_TRC("%s", __func__);

    sis8300llrfControllerChannelGroup *cg = 
        dynamic_cast<sis8300llrfControllerChannelGroup*> (getChannelGroup());

    if ( !_Initialized ||
         cg->getCurrentState() != nds::CHANNEL_STATE_DISABLED  ||
         (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
          _device->getCurrentState() != nds::DEVICE_STATE_ON)) {
        
        NDS_DBG("%s not committing parameters.", _ChanStringIdentifier);
        return ndsSuccess;
    }


    // if frequency or bandwidth change
    if (_FrequencyChanged || _BandwidthChanged)
    {
        status =  updateFilter();
        if (status == ndsSuccess) {
            if (_FrequencyChanged) {
                doCallbacksFloat64(_Frequency, _interruptIdFreq);
                _FrequencyChanged = 0;
            }
            if (_BandwidthChanged) {
                doCallbacksFloat64(_Bandwidth, _interruptIdBwidth);
                _BandwidthChanged = 0;
            }
        }
        return status;
    }

    return sis8300llrfChannel::commitParameters();
}


/**
 * @brief Update the constants from this filter
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This method update the constants form notch filter. It is called
 * when frequency or bandiwith changes, and also when F_sampling or N
 * changes (in this case it is called from sis8300llrfControllerChannelGroup)
 * */

ndsStatus sis8300llrfNotchFilterChannel::updateFilter() {
    epicsFloat64 h, omg0, xi0,  N;

    NDS_TRC("%s", __func__);

    sis8300llrfControllerChannelGroup *cg = 
        dynamic_cast<sis8300llrfControllerChannelGroup*> (getChannelGroup());

    // to avoid division by 0
    if (_Frequency == 0 || cg->getN() == 0)
        return ndsError;

    N = cg->getN();

    h = 1/(_FSampling/N);
    omg0 = 2*M_PI*_Frequency;
    xi0 = _Bandwidth/(2*_Frequency);
    
    _ParamVals[notch_fil_const_a_real] = (double) exp(-(xi0*omg0*h))*cos(omg0*h);
    _ParamVals[notch_fil_const_a_imag] = (double) exp(-(xi0*omg0*h))*sin(omg0*h);
    _ParamVals[notch_fil_const_b_real] = (double) (1-exp(-(xi0*omg0*h)))*cos(omg0*h);
    _ParamVals[notch_fil_const_b_imag] = (double) (1-exp(-(xi0*omg0*h)))*sin(omg0*h);

    _ParamChanges[notch_fil_const_a_real] = 1;
    _ParamChanges[notch_fil_const_a_imag] = 1;
    _ParamChanges[notch_fil_const_b_real] = 1;
    _ParamChanges[notch_fil_const_b_imag] = 1;

    return sis8300llrfChannel::commitParameters();
}
