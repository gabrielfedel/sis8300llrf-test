/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @file sis8300llrLowPassFiltChannel.cpp
 * @brief Implementation of sis8300llrf filter channel in NDS
 * @author gabrielfedel, gabriel.fedel@esss.se
 * @date 12.8.2019
 * 
 * This class holds all the settings related to the low pass filter that are 
 * available on the device. It is more or less a collection of setters and
 * getters, since this unit has no readouts that have to be made on 
 * pulse-to-pulse basis.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"
#include "sis8300llrfLowPassFiltChannel.h"
#include <math.h>

std::string sis8300llrfLowPassFiltChannel::
                    PV_REASON_CONST_A    = "LowPassFiltConstA";
std::string sis8300llrfLowPassFiltChannel::
                    PV_REASON_CONST_B    = "LowPassFiltConstB";
std::string sis8300llrfLowPassFiltChannel::
                    PV_REASON_EN         = "LowPassFiltEn";
std::string sis8300llrfLowPassFiltChannel::
                    PV_REASON_CUTOFF     = "LowPassCutOff";


/**
 * @brief sis8300llrfLowPassFiltChannel constructor
 */
sis8300llrfLowPassFiltChannel::sis8300llrfLowPassFiltChannel(epicsFloat64 FSampling) :
        sis8300llrfChannel(
            SIS8300LLRFDRV_LOW_PASS_FILTER_PARAM_NUM, 
            SIS8300LLRFDRV_LOW_PASS_FILTER_PARAM_INT_FIRST) {

    sprintf(_ChanStringIdentifier, "Low Pass filter");

    _cutOff = 0;
    _cutOffChanged = 0;
    _FSampling = FSampling;
}


/**
 * @brief sis8300llrfLowPassFiltChannel destructor
 */
sis8300llrfLowPassFiltChannel::~sis8300llrfLowPassFiltChannel() {}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfLowPassFiltChannel::readParameter(
                int paramIdx, double *paramVal) {
    return sis8300llrfdrv_get_low_pass_filter_param(_DeviceUser,
            (sis8300llrfdrv_low_pass_filter_param) paramIdx, paramVal);
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfLowPassFiltChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_low_pass_filter_param(_DeviceUser,
                (sis8300llrfdrv_low_pass_filter_param) paramIdx, 
                _ParamVals[paramIdx], paramErr);
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfLowPassFiltChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfLowPassFiltChannel::PV_REASON_CONST_A,
            &sis8300llrfLowPassFiltChannel::setConstantA,
            &sis8300llrfLowPassFiltChannel::getConstantA,
            &_interruptIds[low_pass_fil_const_a]);
 
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfLowPassFiltChannel::PV_REASON_CONST_B,
            &sis8300llrfLowPassFiltChannel::setConstantB,
            &sis8300llrfLowPassFiltChannel::getConstantB,
            &_interruptIds[low_pass_fil_const_b]);

    NDS_PV_REGISTER_INT32(
           sis8300llrfLowPassFiltChannel::PV_REASON_EN,
           &sis8300llrfLowPassFiltChannel::setEnable,
           &sis8300llrfLowPassFiltChannel::getEnable,
           &_interruptIds[low_pass_fil_en]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfLowPassFiltChannel::PV_REASON_CUTOFF,
            &sis8300llrfLowPassFiltChannel::setCutOff,
            &sis8300llrfLowPassFiltChannel::getCutOff,
            &_interruptIDCutOff);


    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ======= RELATED TO LOW PASS FILTER ======== */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set Low Pass filter constant A
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the A constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfLowPassFiltChannel::setConstantA(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[low_pass_fil_const_a] = (double) value;

    _ParamChanges[low_pass_fil_const_a] = 1;
    return commitParameters();
}
/**
 * @brief Get Low Pass filter constant A
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the A constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfLowPassFiltChannel::getConstantA(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[low_pass_fil_const_a];

    return ndsSuccess;
}
/**
 * @brief Set low pass filter constant B
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the B constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfLowPassFiltChannel::setConstantB(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[low_pass_fil_const_b] = (double) value;

    _ParamChanges[low_pass_fil_const_b] = 1;
    return commitParameters();
}
/**
 * @brief Get low pass filter constant B
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the B constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfLowPassFiltChannel::getConstantB(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[low_pass_fil_const_b];

    return ndsSuccess;
}

/**
 * @brief Enable Low Pass filter
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfLowPassFiltChannel::setEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[low_pass_fil_en] = (double) value;

    _ParamChanges[low_pass_fil_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if Low Pass is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfLowPassFiltChannel::getEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[low_pass_fil_en];

    return ndsSuccess;
}

/**
 * @brief Set low pass filter Cut Off value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the Cut Off
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */

ndsStatus sis8300llrfLowPassFiltChannel::setCutOff(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _cutOff = value;

    _cutOffChanged = 1;
    return commitParameters();
}

/**
 * @brief Get low pass filter cut off value
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the cut off value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */

ndsStatus sis8300llrfLowPassFiltChannel::getCutOff(
                asynUser *pasynUser, epicsFloat64 *value) {
    NDS_TRC("%s", __func__);

    *value = _cutOff; 

    return ndsSuccess;
}

/**
 * @brief Overides commitParameters to manage cutOff
 * 
 * @return ndsError    If update or commitParamters return error
 * @return ndsSuccess  
 * 
 * This method is necessary so Frequency and Bandwidth is managed differently
 * from other PVs. These PVs don't have a register to write, instead of they 
 * will set constants a and b.
 */
ndsStatus sis8300llrfLowPassFiltChannel::commitParameters() {
    ndsStatus status;

    NDS_TRC("%s", __func__);

    sis8300llrfControllerChannelGroup *cg = 
        dynamic_cast<sis8300llrfControllerChannelGroup*> (getChannelGroup());

    if ( !_Initialized ||
         cg->getCurrentState() != nds::CHANNEL_STATE_DISABLED  ||
         (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
          _device->getCurrentState() != nds::DEVICE_STATE_ON)) {
        
        NDS_DBG("%s not committing parameters.", _ChanStringIdentifier);
        return ndsSuccess;
    }


    // if frequency or bandwidth change
    if (_cutOffChanged)
    {
        status = updateFilter();
        if (status == ndsSuccess)
            doCallbacksFloat64(_cutOff, _interruptIDCutOff);
        _cutOffChanged = 0;
        return status;
    }

    return sis8300llrfChannel::commitParameters();
}

ndsStatus sis8300llrfLowPassFiltChannel::updateFilter() {
    NDS_TRC("%s", __func__);
    epicsFloat64 omg0, h;
    epicsInt32 N;

    sis8300llrfControllerChannelGroup *cg = 
            dynamic_cast<sis8300llrfControllerChannelGroup*> (getChannelGroup());

    // to avoid division by 0
    if (cg->getN() == 0)
        return ndsSuccess;

    //get N from channel group
    N = cg->getN();

    omg0 = 2*M_PI*_cutOff; 
    h = 1/(_FSampling/N);

     _ParamVals[low_pass_fil_const_a] = (double) -exp(-omg0*h);
     _ParamVals[low_pass_fil_const_b] = (double) 1 - exp(-omg0*h);

    _ParamChanges[low_pass_fil_const_a] = 1;
    _ParamChanges[low_pass_fil_const_b] = 1;

    return sis8300llrfChannel::commitParameters();
}

