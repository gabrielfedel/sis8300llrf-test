/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfAIChannelGroup.h
 * @brief Header file defining the LLRF analog input channel group class.
 * @author urojec
 * @date 30.5.2014
 */

#ifndef _sis8300llrfAIChannelGroup_h
#define _sis8300llrfAIChannelGroup_h

#include "sis8300AIChannelGroup.h"

#define SIS8300LLRFNDS_CHAN_MASK_ALL        0x3ff     /**< Mask for 
                                            enabling all the channels */
#define SIS8300LLRFNDS_MAX_CLK_FREQUENCY    114000000 /**< FPGA cannot 
                                             operate faster than this */

class sis8300llrfAIChannelGroup: public sis8300AIChannelGroup {

public:
    sis8300llrfAIChannelGroup(const std::string& name);
    virtual ~sis8300llrfAIChannelGroup();


    /* Override generic trigger settings, because the LLRF controller has
     * it's own special ones. */
    virtual ndsStatus markAllParametersChanged();

    /* These should not change when controller is running - device in ON 
     * state */
    virtual ndsStatus setClockSource(
                        asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setClockFrequency(
                        asynUser* pasynUser, epicsFloat64 value);
    virtual ndsStatus setClockDivisor(
                        asynUser* pasynUser, epicsInt32 value);

    /* Override unsupported settings */
    virtual ndsStatus setTrigger(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setTriggerCondition(
                        asynUser *pasynUser, const char *data, 
                        size_t numchars, size_t *nbytesTransferead);
    virtual ndsStatus onTriggerConditionParsed(
                        asynUser *pasynUser, const nds::Trigger& trigger);
    virtual ndsStatus setTriggerDelay(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setTriggerRepeat(
                        asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setEnabled(
                        asynUser* pasynUser, epicsInt32 value);

protected:
    /* Override parent handlers, because we do not have a daq task in 
     * the AI group */
    virtual ndsStatus onSwitchProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onLeaveProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterError(
                        nds::ChannelStates from, nds::ChannelStates to);

    /* Override because not supported */
    virtual ndsStatus handleStartMsg(
                        asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleStopMsg(
                        asynUser *pasynUser, const nds::Message& value);
};

#endif /* _sis8300llrfAIChannelGroup_h */
