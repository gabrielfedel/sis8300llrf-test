/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIChannelGroup.cpp
 * @brief Implementation of PI channel group in NDS.
 * @author urojec
 * @date 26.5.2014
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfChannel.h"

#include <math.h>

std::string sis8300llrfControllerChannelGroup::
                PV_REASON_SAMPLES_CNT_PI_RAMP_UP = "SamplesCntPIRampUp";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_SAMPLES_CNT_PI_ACTIVE  = "SamplesCntPIActive";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_SAMPLES_CNT_PI_TOTAL   = "SamplesCntPITotal";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_SAMPLES_CNT_ADC_TOTAL  = "SamplesCntADCTotal";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_OUTPUT_TYPE            = "OutputType";

/**
 * @brief Controller ChannelGroup constructor.
 * @param [in] name Channel Group name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation and @see #sis8300llrfChannelGroup
 */
sis8300llrfControllerChannelGroup::sis8300llrfControllerChannelGroup(
        const std::string& name, sis8300drv_usr *newDeviceUser) :
        sis8300llrfChannelGroup(name, newDeviceUser) {


    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfControllerChannelGroup::onLeaveProcessing, 
        this, _1, _2));

    _TriggerType = (epicsInt32) mlvds_012;
    _TriggerTypeChanged = 1;
    _OutputType  = (epicsInt32) output_drive_pi;
    _OutputTypeChanged = 1;
}

sis8300llrfControllerChannelGroup::~sis8300llrfControllerChannelGroup() {}

double sis8300llrfControllerChannelGroup::_Rad2Deg = 
                                            180.0 / (epicsFloat64) M_PI;

/**
 * @see #sis8300llrfChannelGroup::writeToHardware
 */
ndsStatus sis8300llrfControllerChannelGroup::writeToHardware() {
    int status;

    if (_TriggerTypeChanged) {
        status = sis8300llrfdrv_set_trigger_setup(
                    _DeviceUser, 
                    (sis8300llrfdrv_trg_setup) _TriggerType);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_trigger_setup", status);

        doCallbacksInt32(_TriggerType, _interruptIdTriggerType);

        _TriggerTypeChanged = 0;

        _UpdateReason |= SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS;
    }

    if (_OutputTypeChanged) {
        status = sis8300llrfdrv_set_output_drive_src(
                    _DeviceUser, 
                    (sis8300llrfdrv_output_drive_src) _OutputType);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_output_drive_src", status);

        doCallbacksInt32(_OutputType, _interruptIdOutputType);

        _OutputTypeChanged = 0;

        _UpdateReason |= SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS;
    }

    return sis8300llrfChannelGroup::writeToHardware();
}

/**
 * @see #sis8300llrfChannelGroup::readParamters
 */
ndsStatus sis8300llrfControllerChannelGroup::readParameters() {
    int status;

    sis8300llrfdrv_trg_setup triggerType;

    status = sis8300llrfdrv_get_trigger_setup(_DeviceUser, &triggerType);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_trigger_setup", status);

    doCallbacksInt32((epicsInt32)_TriggerType, _interruptIdTriggerType);

    return ndsSuccess;
}

/**
 * @see #sis8300llrfChannelGroup::markAllParamtersAsChanged
 */
ndsStatus sis8300llrfControllerChannelGroup::markAllParametersChanged() {
    _TriggerTypeChanged = 1;
    _OutputTypeChanged  = 1;

    return sis8300llrfChannelGroup::markAllParametersChanged();
}

/**
 * @brief State handler for leaving PROCESSING state
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Update all the values from the previous pulse
 */
ndsStatus sis8300llrfControllerChannelGroup::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
    
    int status, i;
    unsigned uRegVal;

    /* Read the acquired sample counts from the controller */
    for (i = 0; i < SIS8300LLRFDRV_SAMPLES_CNT_NUM; i++) {
        status = sis8300llrfdrv_get_acquired_nsamples(
                    _DeviceUser, (sis8300llrfdrv_samples_cnt)i, &uRegVal);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_get_samples_cnt", status);
        doCallbacksInt32((epicsInt32) uRegVal, _interruptIds[i]);
    }

    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfControllerChannelGroup::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_SAMPLES_CNT_PI_RAMP_UP,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[samples_cnt_pi_ramp_up_phase]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_SAMPLES_CNT_PI_ACTIVE,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[samples_cnt_pi_active_phase]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_SAMPLES_CNT_PI_TOTAL,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[samples_cnt_pi_total]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_SAMPLES_CNT_ADC_TOTAL,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[samples_cnt_cavity_total]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_OUTPUT_TYPE,
        &sis8300llrfControllerChannelGroup::setOutputType,
        &sis8300llrfControllerChannelGroup::getOutputType,
        &_interruptIdOutputType);

    return sis8300llrfChannelGroup::registerHandlers(pvContainers);
}

/* ======= GETTERS AND SETTERS ======= */

/**
 * @brief set trigger type, see in sis8300llrfdrv.h
 *
 * @param pasynUser asynUser
 * @param value     trigger value
 *
 * @return ndsError   if device is in ON - the control loop is running
 *                    or @see #commitParamters failed
 * @return ndsSuccess Successful set
 */
ndsStatus sis8300llrfControllerChannelGroup::setTriggerType(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (_device->getCurrentState() == nds::DEVICE_STATE_ON) {
        NDS_ERR("Trigger setup cannot be changed when device is in on"
            "or ioc init");
        return ndsError;
    }

    _TriggerType = value;
    _TriggerTypeChanged = 1;

    return commitParameters();
}

/**
 * @brief Set the oputput type for the controller - either PI
 *        or FF driven
 * 
 * @param [in]  pasynUser    Asyn User context struct
 * @param [in]  value        0 for PI and 1 for FF driven output
 * 
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfControllerChannelGroup::setOutputType(
                asynUser *pasynUser, epicsInt32 value) {

    _OutputType = value;
    _OutputTypeChanged = 1;

    return commitParameters();
}
/**
 * @brief Get the oputput type for the controller - either PI
 *        or FF driven
 * 
 * @param [in]  pasynUser    Asyn User context struct
 * @param [out] value        0 for PI and 1 for FF driven output
 * 
 * @return ndsSuccess Always
 */
ndsStatus sis8300llrfControllerChannelGroup::getOutputType(
                asynUser *pasynUser, epicsInt32 *value) {
    *value = _OutputType;
    return ndsSuccess;
}

epicsInt32 sis8300llrfControllerChannelGroup::getN() {
    return _N;
}

void sis8300llrfControllerChannelGroup::setN(epicsInt32 N) {
    sis8300llrfChannel *ch;

    _N = N;
    if (getCurrentState() != nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        //Call update Filter from all channels, so the filters which use
        //N will be updated
        for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
            ch = dynamic_cast<sis8300llrfChannel *>(iter->second); 
            ch->updateFilter();
       }
    }  
}
