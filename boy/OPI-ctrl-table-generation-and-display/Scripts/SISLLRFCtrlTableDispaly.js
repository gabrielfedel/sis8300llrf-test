importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pv = PVUtil.getString(pvArray[0]);

if (pv == "SP-SPECMODE" || pv == "FF-SPECMODE") {
	widget.setPropertyValue("trace_count",4);
	widget.setPropertyValue("trace_3_y_pv", "$(Device2Macro):$(Channel2Macro):ANG");
	widget.setPropertyValue("trace_2_y_pv", "$(Device2Macro):$(Channel2Macro):MAG");
}
else {
	widget.setPropertyValue("trace_count",2);
	widget.setPropertyValue("trace_3_y_pv", "loc://SISLLRFParam2");
	widget.setPropertyValue("trace_2_y_pv", "loc://SISLLRFParam2");
}