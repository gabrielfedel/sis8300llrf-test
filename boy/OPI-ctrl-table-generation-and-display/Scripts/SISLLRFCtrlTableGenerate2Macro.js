importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);

var pv1 = PVUtil.getString(pvArray[0]);
macroInput.put("Channel2Macro", pv1);

var pv2 = PVUtil.getString(pvArray[1]);
macroInput.put("Param22Macro", pv2);

var pv3 = PVUtil.getString(pvArray[2]);
macroInput.put("Param32Macro", pv3);

var pv4 = PVUtil.getString(pvArray[3]);
macroInput.put("Param42Macro", pv4);

var pv5 = PVUtil.getString(pvArray[4]);
macroInput.put("Param52Macro", pv5);

var pv6 = PVUtil.getString(pvArray[5]);
macroInput.put("Param62Macro", pv6);


widgetController.setPropertyValue("macros", macroInput);
