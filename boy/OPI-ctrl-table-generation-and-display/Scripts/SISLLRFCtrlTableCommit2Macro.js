importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);

var pv1 = PVUtil.getString(pvArray[0]);
macroInput.put("TableGrp2Macro", pv1);

widgetController.setPropertyValue("macros", macroInput);
