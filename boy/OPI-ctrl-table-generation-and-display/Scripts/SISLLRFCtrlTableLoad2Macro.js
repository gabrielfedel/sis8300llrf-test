importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);

var pv1 = PVUtil.getString(pvArray[0]);
macroInput.put("Channel2Macro", pv1);

var pv2 = PVUtil.getString(pvArray[1]);
macroInput.put("InputFileName2Macro", pv2);

widgetController.setPropertyValue("macros", macroInput);
