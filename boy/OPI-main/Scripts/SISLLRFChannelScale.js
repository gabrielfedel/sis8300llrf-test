importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pv = PVUtil.getString(pvArray[0]);
//var mac = widget.getMacroValue("SISLLRFChannel2Macro");
//ConsoleUtil.writeInfo(mac);

/*
if (pv == "PI-ANG-ERR") {
	widget.setPropertyValue("axis_1_minimum", -185);
	widget.setPropertyValue("axis_1_maximum", 185);
	widget.setPropertyValue("axis_1_auto_scale",false);
	widget.setPropertyValue("axis_1_axis_title","deg");
}
else if (pv == "PI-MAG-ERR") {
	widget.setPropertyValue("axis_1_minimum", -5);
	widget.setPropertyValue("axis_1_maximum", 105);
	widget.setPropertyValue("axis_1_auto_scale",false);
	widget.setPropertyValue("axis_1_axis_title","%");
}
*/
if (pv == "PI-I:ERR" || pv == "PI-Q:ERR") {
	widget.setPropertyValue("axis_1_minimum", -1.01);
	widget.setPropertyValue("axis_1_maximum", 1.01);
	widget.setPropertyValue("axis_1_auto_scale",false);
}
else if (pv == "AI0" || pv == "AI1" || pv == "A21" || pv == "AI3" || pv == "AI4" || pv == "AI5") {
	widget.setPropertyValue("axis_1_minimum", -1.01);
	widget.setPropertyValue("axis_1_maximum", 1.01);
	widget.setPropertyValue("axis_1_auto_scale",false);
}
else {
	widget.setPropertyValue("axis_1_auto_scale",true);
	widget.setPropertyValue("axis_1_axis_title","");
}
